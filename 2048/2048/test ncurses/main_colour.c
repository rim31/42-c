/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_colour.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 13:15:23 by oseng             #+#    #+#             */
/*   Updated: 2016/01/30 14:49:08 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//couleurs : yellow, green, blue, red, magenta, white...

#include <ncurses.h>

int	main()
{
	int key;
	initscr();//lance l'ecran
	raw();	//ca va avec le lancement d'ecran

	start_color();//initialisation de la couleur
	init_pair(1, COLOR_RED, COLOR_BLUE);//on initialise la paire de couleur au chiffre 1, texte rouge, fond bleu
	attron(COLOR_PAIR(1));//on active la coloration 1
	printw("shake ur booty");
	attroff(COLOR_PAIR(1));//on arrete la coloration
	key = getch();//pause le pgrm en touchant le clavier
	if (key != 0)
		printw("%d", key);
	endwin();//ferme la fenentre

	return (0);
}
