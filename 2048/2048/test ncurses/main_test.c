/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 13:06:22 by oseng             #+#    #+#             */
/*   Updated: 2016/01/30 13:15:10 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//attributes : background and underlignes
#include <ncurses.h>


int	main()
{
	initscr();
	raw();
	attron(A_STANDOUT | A_UNDERLINE);
	mvprintw(12, 40, "Read this ma Gueule");//affiche sur les coordonne voulu
	attroff(A_STANDOUT | A_UNDERLINE);


	getch();
	endwin();
	return (0);
}



