//#include "2048.h"
#include <ncurses.h>
#include "libft/libft.h"

void ft_resol(int **grid)
{
  int a;
  int b;
  a = 0;
  while (a < 4)
  {
    b = 0;
    while (b < 4)
    {
      ft_putnbr(grid[a][b]);
      ft_putchar(' ');
      b++;
    }
    a++;
    ft_putchar('\n');
  }
}
