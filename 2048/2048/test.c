/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 17:06:53 by oseng             #+#    #+#             */
/*   Updated: 2016/01/30 18:47:54 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include "game2048.h"
#include <ncurses.h>
#include "libft/libft.h"

int		main(void)
{
	int x;
	int y;
	int kt;
	int	a;
	int	b;

	a = 0;
	b = 0;
	initscr();//lance l'ecran
	x = COLS / 2;//max de colonne directement generer en macro avec initscr
	y = LINES / 2;//max de colonne directement generer en macro avec initscr
	keypad(stdscr, TRUE);//active le clavier
	noecho();//n'affiche pas tout ce que tu tapes
	mvprintw(y, x, "(-o-)");//affiche le vaisseau de star wars
	while (kt != 27)//tant qu'on a pas appuyer sur ESC
	{
		mvprintw(0, 0, "Press ESC key to exit");//affichage permaent en haut a gauche
		mvprintw(0, (COLS / 2) - ft_strlen("Star Wars"), "Star Wars");//affchage en haut centree
		kt = getch();//enregistre les touches tapees
		if (kt == KEY_RIGHT)//macro de touche clavier deja active avec keypad
			x+=1;//on deplace a droite
		if (kt == KEY_LEFT)
			x-=1;
		if (kt == KEY_DOWN)
			y+=1;
		if (kt == KEY_UP)
			y-=1;
		if (kt == KEY_UP)
		{
			a = x;
			b = y;
		}
		if (b > 0)
		{
			mvprintw(b - 1, a + 3, "|");
			b--;
		}
		clear();//pour effacer la remanence
		mvprintw(y, x, "(-o-)");//affichage du vaisseau selon les coordonnees
		refresh();//reactualise la fenetre
	}
	endwin();
	return (0);
}
