#include "libft/libft.h"

int **ft_grid(int nb);

int main(int argc, char const *argv[])
{
  int **grid;
  grid = ft_grid(4);

  int a;
  int b;
  a = 0;
  while (a < 4)
  {
    b = 0;
    while (b < 4)
    {
      ft_putnbr(grid[a][b]);
      ft_putchar(' ');
      b++;
    }
    a++;
    ft_putchar('\n');
  }
  return 0;
}
