/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_grid.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 10:10:36 by oseng             #+#    #+#             */
/*   Updated: 2016/01/30 16:57:11 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"

/*
**exemple : int Tableau [3][4]
**On peut représenter un tel tableau de la manière suivante :
**Tableau[0][0]	Tableau[0][1]	Tableau[0][2]	Tableau[0][3]
**Tableau[1][0]	Tableau[1][1]	Tableau[1][2]	Tableau[1][3]
**Tableau[2][0]	Tableau[2][1]	Tableau[2][2]	Tableau[2][3]
 */

int **ft_grid(int nb)
{
  int **grid;
  int a;
  int b;

  if (!(grid = (int**)malloc(sizeof(int*)*(nb - 1))))
    return (NULL);
  if (nb != 4 && nb != 5)
    return (0);
  a = 0;
  while (a < nb)
  {
    b = 0;
    grid[a] = (int*)malloc(sizeof(int)*(nb));
    while (b < nb)
    {
      grid[a][b] = 0;
      b++;
    }
    a++;
  }
  return (grid);
}
