#include "game_2048.h"
#include "libft/libft.h"

int **ft_grid(int nb);
int **ft_right(int **grid);
void	ft_resol(int **grid);
int	**ft_right(int **grid);

int main(int argc, char const *argv[])
{
	int **grid;
	grid = ft_grid(4);

	int a;
	int b;
	ft_resol(grid);
	grid[0][0] = 4;
	grid[1][0] = 2;
	grid[2][3] = 8;
	grid[1][2] = 2;
	ft_putchar('\n');
	t_coord	pt;
	pt.x = 1;
	pt.y = 1;
	grid[pt.y][pt.x] = 4;
//	ft_resol(grid);

//	ft_putchar('\n');
	ft_resol(grid);
	grid = ft_right(grid);

	ft_putchar('\n');
	ft_resol(grid);
	return 0;
}
