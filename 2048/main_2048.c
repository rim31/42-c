//#include "2048.h"
#include <ncurses.h>
#include "libft/libft.h"

int **ft_algo(int **grid);
void ft_resol(int **grid);
int **ft_grid(int nb);

int main(int argc, char const *argv[])
{
  int **grid;

  grid = ft_grid(4);
  grid[2][2] = 2;
  grid[1][3] = 4;
  ft_resol(grid);

  return 0;
}
