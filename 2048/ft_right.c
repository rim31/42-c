#include "game_2048.h"
#include "libft/libft.h"

/*typedef	struct	s_coord;
  struct		s_coord
  {
  int 	x;
  int 	y;
  int 	i;
  int 	j;
  int 	score;
  }		t_coord;*/
void ft_resol(int **grid);

static void	ft_swap(int *a, int *b)
{
	int tmp;

	tmp = 0;
	tmp = *a;
	*a = *b;
	*b = tmp;
}

int **ft_right(int **grid)
{
	t_coord	pt;

	pt.y = 0;
	while (0 <= pt.y && pt.y < 4)
	{
		pt.i = -1;
		pt.x = 3;
		while (0 <= pt.x && pt.x < 4)
		{
			if (grid[pt.y][pt.x] != 0)
			{
				while (grid[pt.y][pt.x + pt.i] == 0 && pt.x + pt.i < 4 && 0 <= pt.x + pt.i)
					pt.i--;
				if (grid[pt.y][pt.x + pt.i + 1] == 0)
					ft_swap(&grid[pt.y][pt.y], &grid[pt.y][pt.x + pt.i]);
			}
			pt.x--;
		}
		pt.y++;
	}	
	ft_resol(grid);
	return (grid);
}
/*

   while (pt.y < 4)
   {
   pt.x = 0;
   while (pt.x < 4)
   {
   grid[pt.y][pt.x] = 1;
   pt.x++;
   }
   pt.y++;
   }	

 */
