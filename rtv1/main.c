/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 18:24:02 by oseng             #+#    #+#             */
/*   Updated: 2016/04/18 09:48:06 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static t_env	ft_init(t_env *a)
{
	a->x = 0;
	a->y = 0;
	a->pas = 30;
	a->xx = 0;
	a->yy = 0;
	a->k0 = 1000000;
	a->keyup = 0;
	a->keydown = 0;
	a->keyleft = 0;
	a->keyrigth = 0;
	ft_init_plan(a);
	return (*a);
}

/*
** need a parser per haps
*/

int				main(int argc, char **argv)
{
	t_env		*a;

	ft_putstr("\033[92mEnter the number of scene : 1 <-> 9\n\033[0m");
	if (argc == 2)
	{
		a = malloc(sizeof(t_env));
		if (ft_atoi(argv[1]) >= 1 && ft_atoi(argv[1]) <= 9)
		{
			ft_putstr("\033[92mBONUS: multi ligths, brigthness\n\033[0m");
			*a = ft_init(a);
			ft_start_screen(*a);
		}
		else
			ft_putstr("\033[96mERROR :-P\n\033[0m");
		free(a);
	}
	return (0);
}
