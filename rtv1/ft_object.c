/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_object.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 18:24:02 by oseng             #+#    #+#             */
/*   Updated: 2016/04/19 16:33:19 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

/*
** mouvement des objets en fct des coordonnees
** -x : vers nous, + x : vers le fond
** -y : vers la gauche, +y : vers la droite
** -Z : descend,  +z monte
*/

t_env ft_init_plan(t_env *a)
{
	a->cam.x = -300;
	a->cam.y = 0;
	a->cam.z = 0;
	a->angle.x = 0.0;
	a->angle.y = 0.0;
	a->angle.z = 0.0;

	a->lum = (t_vect *)malloc(sizeof(t_vect));
	a->lum[0].x = -100;
	a->lum[0].y = 100;
	a->lum[0].z = 100;

	a->obj = (t_obj *)malloc(sizeof(t_obj) * 7);//nombre d'objet + 1
	a->obj[0].name = "plan";
	// a->obj[0].color = 0xCCF000;
	a->obj[0].color.x = 0xFF;
	a->obj[0].color.y = 0xFF;
	a->obj[0].color.z = 0x00;
	a->obj[0].center.x = 0.21;
	a->obj[0].center.y = 0.21;
	a->obj[0].center.z = -1;
	a->obj[0].d = 1;

	a->obj[1].name = "sphere";
	// a->obj[1].color = 0xFF0000;
	a->obj[1].color.x = 255;
	a->obj[1].color.y = 0x00;
	a->obj[1].color.z = 0x00;
	a->obj[1].center.x = 0.0;
	a->obj[1].center.y = -30.0;
	a->obj[1].center.z = 5.0;
	a->obj[1].d = 20;

	a->obj[2].name = "cylindre";
	// a->obj[2].color = 0xFF01F0;
	a->obj[2].color.x = 0x0F;
	a->obj[2].color.y = 0xFE;
	a->obj[2].color.z = 0x00;
	a->obj[2].center.x = -50.0;//3.0;
	a->obj[2].center.y = 50;//-10.0;
	a->obj[2].center.z = 0;//10.0;
	a->obj[2].angle.x = M_PI / 4;
	a->obj[2].angle.y = 0;//M_PI / 4;
	a->obj[2].angle.z = M_PI / 9;
	a->obj[2].d = 2;

	// a->obj[3].name = "cylindre";
	// // a->obj[3].color = 0xFF0100;
	// a->obj[3].color.x = 0x00;
	// a->obj[3].color.y = 0x0D;
	// a->obj[3].color.z = 0xFF;
	// a->obj[3].center.x = -10.0;//3.0;
	// a->obj[3].center.y = 100.0;
	// a->obj[3].center.z = 0;//10.0;
	// a->obj[3].angle.x = 0;//M_PI / 9;
	// a->obj[3].angle.y = -M_PI / 4;
	// a->obj[3].angle.z = 0;//M_PI / 9;
	// a->obj[3].d = 5;

	a->obj[3].name = "cone";
	// a->obj[3].color = 0xFC90CC;
	a->obj[3].color.x = 0xFF;
	a->obj[3].color.y = 0x00;
	a->obj[3].color.z = 0xCC;
	a->obj[3].center.x = 0.0;
	a->obj[3].center.y = 40.0;
	a->obj[3].center.z = 40.0;
	a->obj[3].angle.x = M_PI / 9;
	a->obj[3].angle.y = 0;//-M_PI / 2;
	a->obj[3].angle.z = 0;//M_PI / 4;
	a->obj[3].d = 1;

//////////////////////////////////////////////

	// a->obj[4].name = "sphere";
	// // a->obj[4].color = 0xCC0000;
	// a->obj[4].color.x = 0xFF;
	// a->obj[4].color.y = 0xA0;
	// a->obj[4].color.z = 0x0F;
	// a->obj[4].center.x = -70.0;
	// a->obj[4].center.y = 10.0;
	// a->obj[4].center.z = 50.0;
	// a->obj[4].d = 10;
	a->obj[4].name = "plan";
	// a->obj[4].color = 0xCCF000;
	a->obj[4].color.x = 0xFF;
	a->obj[4].color.y = 0xFF;
	a->obj[4].color.z = 0x00;
	a->obj[4].center.x = 1.01;
	a->obj[4].center.y = 0.21;
	a->obj[4].center.z = 0.01;
	a->obj[4].d = -10;

	a->obj[5].name = "cylindre";
	// a->obj[5].color = 0xFF0100;
	a->obj[5].color.x = 0x00;
	a->obj[5].color.y = 0x0D;
	a->obj[5].color.z = 0xFF;
	a->obj[5].center.x = -90.0;//3.0;
	a->obj[5].center.y = 50;//-10.0;
	a->obj[5].center.z = 0;//10.0;
	a->obj[5].angle.x = 0;//M_PI / 9;
	a->obj[5].angle.y = 0;//-M_PI / 4;
	a->obj[5].angle.z = M_PI / 9;
	a->obj[5].d = 5;

	// a->obj[6].name = "cone";
	// a->obj[6].color = 0xFF0100;
	// a->obj[6].center.x = 0.0;//3.0;
	// a->obj[6].center.y = 0;//-10.0;
	// a->obj[6].center.z = 0;//10.0;
	// a->obj[6].angle.x = M_PI / 4;
	// a->obj[6].angle.y = 0;//-M_PI / 4;
	// a->obj[6].angle.z = -M_PI / 9;
	// a->obj[6].d = 2;

	return(*a);
}

// a->plan = (t_obj *)malloc(sizeof(t_obj));
// a->plan[0].name = "plan";
// a->plan[0].color = 0xCCF000;
// a->plan[0].center.x = 0.08;
// a->plan[0].center.y = 0.05;
// a->plan[0].center.z = -1.0;
// a->plan[0].d = 1;
//
// a->sphere = (t_obj *)malloc(sizeof(t_obj));
// a->sphere[0].name = "sphere";
// a->sphere[0].color = 0xFF0000;
// a->sphere[0].center.x = 0;
// a->sphere[0].center.y = 0;
// a->sphere[0].center.z = 0;
// a->sphere[0].d = 20;
//
// a->cylind = (t_obj *)malloc(sizeof(t_obj));
// a->cylind[0].name = "cylindre";
// a->cylind[0].color = 0xFF01F0;
// a->cylind[0].center.x = 3;
// a->cylind[0].center.y = -10.0;
// a->cylind[0].center.z = 10.0;
// a->cylind[0].angle.x = M_PI / 4;
// a->cylind[0].angle.y = -M_PI / 4;
// a->cylind[0].angle.z = 0;//-M_PI / 9;
// a->cylind[0].d = 15;
//
// a->cone = (t_obj *)malloc(sizeof(t_obj));
// a->cone[0].name = "cone";
// a->cone[0].color = 0xFC90CC;
// a->cone[0].center.x = 0;
// a->cone[0].center.y = 0;
// a->cone[0].center.z = 0;
// a->cone[0].angle.x = 0;//M_PI / 3;
// a->cone[0].angle.y = 0;//+M_PI / 6;
// a->cone[0].angle.z = -M_PI / 4;
// a->cone[0].d = 4;
