/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   my_key_funct.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 18:24:02 by oseng             #+#    #+#             */
/*   Updated: 2016/04/13 12:02:52 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void		ft_rotation(int keycode, t_env *e)
{
	e->pas = e->pas;//pour le -Wall -Wextra -Werror
	if (keycode == 53)
		exit(0);
}

int				my_key_funct(int keypress, t_env *a)
{
	if (keypress == 53)
		exit(0);
	if (keypress == 125)
		a->keyup = 1;
	else if (keypress == 126)
		a->keydown = 1;
	if (keypress == 123)
		a->keyleft = 1;
	else if (keypress == 124)
		a->keyrigth = 1;
	if (keypress == 1 || keypress == 49 || keypress == 0)
		ft_rotation(keypress, a);
		ft_bzero((void *)a->data, (H * L * 4) - 1);
	return (0);
}

int				my_keyrelease_funct(int keyrelease, t_env *a)
{
	if (keyrelease == 125)
		a->keyup = 0;
	else if (keyrelease == 126)
		a->keydown = 0;
	if (keyrelease == 123)
		a->keyleft = 0;
	else if (keyrelease == 124)
		a->keyrigth = 0;
	return (0);
}

// /*
// **possibility to see up/down with the mouse : e->updown = -(y - e->higth / 2);
// */
//
// int				my_mouse_fct(int x, int y, t_env *e)
// {
// 	double		center;
// 	double		oldd;
// 	double		oldp;
//
// 	if (x > 0 && x < L && y > 0 && y < H)
// 	{
// 		center = (x - L / 2) / H;
// 		oldd = e->dirx;
// 		e->dirx = e->dirx * cos(-center) - e->diry * sin(-center);
// 		e->diry = oldd * sin(-center) + e->diry * cos(-center);
// 		oldp = e->planx;
// 		e->planx = e->planx * cos(-center) - e->plany * sin(-center);
// 		e->plany = oldp * sin(-center) + e->plany * cos(-center);
// 	}
// 	return (0);
// }
//
// int				my_mouse_funct(int button, int x, int y, t_env *e)
// {
// 	int			a;
// 	int			b;
//
// 	if (button == 1)
// 	{
// 		mlx_string_put(e->mlx, e->win, x, y, 0xff00ff, "pan");
// 		a = (int)L;
// 		b = (int)H;
// 		usleep(250);
// 	}
// 	return (0);
// }
