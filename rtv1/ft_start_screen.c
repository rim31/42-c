/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_start_screen.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 18:42:23 by oseng             #+#    #+#             */
/*   Updated: 2016/04/18 11:41:55 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int 		ft_move(t_env *a)
{
        if (a->keyup == 1)
            a->yy -= a->pas;
        else if (a->keydown == 1)
            a->yy += a->pas;
		if (a->keyrigth == 1)
			a->xx += a->pas;
		else if (a->keyleft == 1)
			a->xx -= a->pas;
		ft_expose(a);
        return (0);
}

void 			ft_error(void)
{
	ft_putendl("ERROR :-P");
	exit(0);
}

void	ft_start_screen(t_env a)
{
	if (a.x > L || a.x < 0 || a.y > H || a.y < 0 || H < 1 || L < 1)
		ft_error();
	a.mlx = mlx_init();
	a.win = mlx_new_window(a.mlx, L, H, "mlx 42");
	a.image = mlx_new_image(a.mlx, L, H);
//	mlx_mouse_hook(a.win, my_mouse_funct, &a);
//	mlx_hook(a.win, 6, 0, my_mouse_fct, &a);
	mlx_hook(a.win, 2, (1L << 0), my_key_funct, &a);
	mlx_hook(a.win, 3, (1L << 1), my_keyrelease_funct, &a);
	mlx_loop_hook(a.mlx, ft_move, &a);
    // ft_expose(&a);//
	mlx_loop(a.mlx);
}
