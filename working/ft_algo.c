/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 16:12:16 by oseng             #+#    #+#             */
/*   Updated: 2016/01/06 15:15:55 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

static	int	ft_sqrt(int nb);
/*fonction qui efface un tetromino lors du backtracking*/
void	ft_erase(int i);
/*focntion dessine un carre vide de . et \n*/
char	*ft_empty_sq(char *save);
char	**ft_view_bdd(char *name);
char	*ft_resize(int nbr);
	int	i;
tab = ft_view_bdd(ft_itoa(i));

/*backtracking*/
int		ft_algo(char *sol, int a)
{
	int	i;
	int	save[] = {11, 8, 3, 4, 5, 6, 7, 8, '\0'};//exemle de pieces a ranger dans un carré

	/*tableau avec les coordonees des pieces selon nous ^^*/
	if (sol[a] == '\0')// aussi a == ft_strlen(sol)) si on arrive au bout de notre gd carré, FINI
		return (1);
	if (sol[a] !=  '.' || sol[a] != '\n')//on continue a avancer dans notre carré
		return (ft_algo(sol, a + 1));
	i = 0;
	while ((tab = ft_view_bdd(ft_itoa(i))) != 0)/*on parcours toute notre tab de sauvegarde des pieces*/
	{
		if (sol[a] == '.' && sol[a + ft_atoi(tab[0])] == '.' 
				&& sol[a + ft_atoi(tab[1])] == '.' && sol[a + ft_atoi(tab[2])] =='.')
		{//on teste si la piece i rentre dans la sol, si oui, on inject la LETTRE
			sol[a] = 'A' + i;
			sol[a + ft_atoi(tab[0])] = 'A' + i;
			sol[a + ft_atoi(tab[1])] = 'A' + i;
			sol[a + ft_atoi(tab[2])] = 'A' + i;
			if (ft_algo(sol, a)) //on teste la recursion
				return (1);
		}
		i++;//on augemente la LETTRE ( posisiton dans le tableau save des pieces
	}
	ft_erase(i);// la recursion ne fonctionne pas, on efface la piece recemment inseree
	ft_resize(1);//on agrandi le carre de 1
	return (0);//fail, on relance la recursion
}






/*

	X = ft_sqrt((char)save) + 1;


int tab[19][3] = {{X + 5, 1, X + 4} , {X + 4, 1, X + 5}, {1, 1, X + 4} ,
	{X + 4, 1, 1} , {X + 5, X + 5, 1} , {1, 1, X + 3} , {1, X + 5, X + 5} ,
	{X + 4, 1, 1} , {X + 5, X + 4, 1} , {X + 5, 1, 1} , {1, X + 4, X + 5} ,
	{1, 1, X + 5} , {1, X + 4, 1} , {X + 5, X + 5, X + 5} , {1, 1, 1} , 
	{1, X + 5, 1} , {1, X + 4, 1} , {X + 5, 1, X + 5} , {X + 4, 1, X + 4}};


+ tab[save[i]][1]] == '.' && sol[a + tab[save[i]][1]] + tab[save[i]][2] == '.' && sol[a + tab[save[i]][2] + tab[save[i]][3]] == '.')
{
	sol[a] = 'A' + i;
	sol[a + save[i][1]] = 'A' + i;
	sol[a + save[i][1]] + save[i][2]] = 'A' + i;
	sol[save[i][2]] + save[i][3]] = 'A' + i;//ft_putchar('A' + i);
}
}
}
}
*/
