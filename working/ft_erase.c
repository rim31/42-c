/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_erase.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 13:31:57 by oseng             #+#    #+#             */
/*   Updated: 2016/01/05 13:33:43 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*fonction qui efface un tetromino lors du backtracking*/
void    ft_erase(int i)
{
	int x;
	x = 0;
	while (sol[x] != '\0')
	{
		if (sol[x] == ('A' + i))/*on remplace chacun des lettre des tetrominos*/
			sol[x] = '.';/*par des '.'*/
		x++;
	}
}

