#include "get_next_line.h"

int             ft_strlen_int(int **s)
{
        int             i;

        i = 0;
        while (s[i][0])
                i++;
        return (i);
}


int    *ft_coord(char const *s)
{
        size_t  a;
        int    *tab;
        size_t  i;
        int             begin;
        int             end;

        a = 0;
        i = 0;
        //tab = NULL;
        if (s && (tab = (int *)malloc(sizeof(*tab) * (ft_strlen(s) + 1))))//fuite memoire
        {
                while (s[i] != '\0')
                {
                        while (s[i] == ' ' && s[i])
                                i++;
                        begin = i;
                        while (s[i] != ' ' && s[i])
                                i++;
                        end = i;
                        if (end > begin)
                                tab[a] = ft_atoi(ft_strsub(s, begin, (end - begin)));
			ft_putnbr(tab[a]);
			ft_putchar(' ');
			a++;
                }
                //tab[a] = NULL;
	ft_putchar('\n');
        }
        return (tab);
}

int **ft_grid(int x, int y)
{
  int **grid;
  int a;
  int b;

  if (!(grid = (int**)malloc(sizeof(int*)*(x - 1))))
    return (NULL);
  b = 0;
  while (b < y - 1)
  {
    a = 0;
    grid[a] = (int*)malloc(sizeof(int)*(x));
    while (a < x)
    {
      grid[a][b] = 0;
	ft_putchar('.');
      a++;
    }
    b++;
  }
  return (grid);
}
