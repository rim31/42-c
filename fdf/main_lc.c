/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 11:45:40 by oseng             #+#    #+#             */
/*   Updated: 2016/01/28 17:18:09 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

typedef	struct	s_env 
{
	void	*mlx;
	void	*win;
	int		i;
	int		j;

}				t_env;

int my_mouse_funct(int button, int x, int y, t_env *e)
{
	printf("boutton %d :\nx : %d\ny : %d\n", button, x, y);
	return(0);
}	


int	my_key_funct(int keycode, t_env *e)
{
	printf("key event %d\n", keycode);
	if (keycode == 53) //si on appuie sr la touche echap
		exit (0);
	return (0);
}

int main()
{
	/*void *mlx;
	void *win;//identifiant de la fenetre pour savoir sur laquelle on travaille
	*/
	static	int	 x;
	static	int	 y;
	t_env e;

	e.mlx = mlx_init();
	e.win = mlx_new_window(e.mlx, 400, 400, "mlx 42");
	y = 50;
	while (y < 150)
	{
		x = 50;
		while (x < 160)
		{
			x > 100 ? mlx_pixel_put(e.mlx, e.win, x, y, 0x15FF00FF) : mlx_pixel_put(e.mlx, e.win, x, y, 0x9908080);
			x++;
		}
		y++;
	}
	mlx_mouse_hook(e.win, my_mouse_funct, &e);
	mlx_key_hook(e.win, my_key_funct, &e); //des qu'un event clavier intervient, on appelera l'autre fct
	mlx_loop(e.mlx); //boucle pour prendre la main, prevoir un touche ESC pour exit ();	
	
}
