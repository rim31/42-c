/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 10:29:45 by oseng             #+#    #+#             */
/*   Updated: 2016/01/29 10:30:31 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 8
# include "libft/libft.h"
# include <fcntl.h>

int	get_next_line(int const fd, char **line);
int    *ft_coord(char const *s);
int             ft_strlen_int(int **s);
int **ft_grid(int x, int y);

#endif
