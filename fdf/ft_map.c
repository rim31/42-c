/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 09:27:37 by oseng             #+#    #+#             */
/*   Updated: 2016/01/29 10:20:48 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int    **ft_map(char const *s, char c)
{
	size_t  		a;
	size_t			b;
	size_t  		i;
	int             begin;
	int             end;
	int    			**tab;

	a = 0;
	i = 0;
	tab = NULL;
	if (s && (tab = (char **)malloc(sizeof(*tab) * (ft_strlen(s) + 1))))
	{
		while (s[i] != '\0')
		{
			while (s[i] == c && s[i])
				i++;
			begin = i;
			while (s[i] != c && s[i])
				i++;
			end = i;
			if (end > begin)
				{
					tab[a][b++] = ft_atoi(ft_strsub(s, begin, (end - begin)));
					ft_putnbr(tab[a][b]);
				}
			if (s[i] == '\n')
			{
				a++;
				b = 0;
			}
		}
		//tab[a][b] = NULL;
	}
	return (tab);
}
