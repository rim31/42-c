/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 11:45:40 by oseng             #+#    #+#             */
/*   Updated: 2016/01/27 16:59:35 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>

int main()
{
	void *mlx;
	void *win;//identifiant de la fenetre pour savoir sur laquelle on travaille
	int	 x;
	int	 y;

	mlx = mlx_init();
	win = mlx_new_window(mlx, 400, 400, "mlx 42");
	y = 50;
	while (y < 150)
	{
		x = 50;
		while (x < 160)
		{
			x > 100 ? mlx_pixel_put(mlx, win, x, y, 0x15FF00FF) : mlx_pixel_put(mlx, win, x, y, 0x9908080);
			x++;
		}
		y++;
	}
	mlx_loop(mlx); //boucle pour prendre la main, prevoir un touche ESC pour exit ();	
	
}
