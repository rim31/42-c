/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 10:29:45 by oseng             #+#    #+#             */
/*   Updated: 2016/01/26 11:35:28 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

//int		get_next_line(int const fd, char **line);

int main(int argc, char const **argv)
{
  int fd;
  int a;
  char *line;
  int	ret;

  a = 0;
  if (argc > 2)
    return (0);
  fd = 0;
  if (argc == 2)
  {
    if (argv[1] == NULL)
      return (0);
    if ((fd = open(argv[1], O_RDONLY)) == -1)
      return (0);
  }
  while ((ret = get_next_line(fd, &line)) > 0)
  {

	  ft_putstr(line);
  }
  //ft_putnbr(ret);
  close (fd);
  return (0);
}
