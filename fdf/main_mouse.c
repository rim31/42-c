/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 11:45:40 by oseng             #+#    #+#             */
/*   Updated: 2016/01/29 17:40:11 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include "libft/libft.h"

typedef	struct	s_env 
{
	void	*mlx;
	void	*win;
	int		i;
	int		j;

}				t_env;

int ft_plot(int a, int b, t_env *e)
{
        int dx;
        int dy;
        int i;
        int     xinc;
        int     yinc;
        int     cumul;
        int x;
        int y;

        x = a;
        y = b;
        dx = e->i - a ;
        dy = e->j - b ;
        xinc = ( dx > 0 ) ? 1 : -1 ;
        yinc = ( dy > 0 ) ? 1 : -1 ;
        dx = abs(dx) ;
        dy = abs(dy) ;
		ft_putnbr(dx);
        mlx_pixel_put(e->mlx, e->win, x, y, 0x15FFFFFF);
        i = 1;
        if ( dx > dy )
        {
                cumul = dx / 2 ;
                while (i <= dx)
                {
                        x = xinc + x;
                        cumul = dy + cumul;
                        if ( cumul >= dx )
                        {
                                cumul -= dx ;
                                y += yinc ;
                        }
                        mlx_pixel_put(e->mlx, e->win, x, y, 0x15FF00FF);
                        i++;
                }
        }
        else
        {
                cumul = dy / 2 ;
                while (i <= dy)
                {
                        y = yinc + y;
                        cumul = dx + cumul;
                        if (cumul >= dy)
                        {
                                cumul -= dy;
                                x = xinc + x;
                        }
                        mlx_pixel_put(e->mlx, e->win, x, y, 0x15FF0000);
                        i++;
                }
        }
       return(0);
}


int my_mouse_funct(int button, int x, int y, t_env *e)
{
	printf("avant :\ni : %d\nj : %d\n", e->i, e->j);
	printf("boutton %d :\nx : %d\ny : %d\n", button, x, y);
	if (x || y)
	{
		e->i = x;
		e->j = y;
		ft_plot(x, y, e);
	}

	return(0);
}	

int	my_key_funct(int keycode, t_env *e)
{
	printf("key event %d\n", keycode);
	if (keycode == 53) //si on appuie sr la touche echap
		exit (0);
	return (0);
}

int main()
{
	/*void *mlx;
	void *win;//identifiant de la fenetre pour savoir sur laquelle on travaille
	*/
	static	int	 x;
	static	int	 y;
	t_env e;

	e.mlx = mlx_init();
	e.win = mlx_new_window(e.mlx, 400, 400, "mlx 42");
	e.i = 0;
	e.i = 0;
	y = 50;
	while (y < 150)
	{
		x = 50;
		while (x < 160)
		{
			x > 100 ? mlx_pixel_put(e.mlx, e.win, x, y, 0x15FF00FF) : mlx_pixel_put(e.mlx, e.win, x, y, 0x9908080);
			x++;
		}
		y++;
	}
	mlx_mouse_hook(e.win, my_mouse_funct, &e);
	mlx_key_hook(e.win, my_key_funct, &e); //des qu'un event clavier intervient, on appelera l'autre fct
	mlx_loop(e.mlx); //boucle pour prendre la main, prevoir un touche ESC pour exit ();	
	
}
