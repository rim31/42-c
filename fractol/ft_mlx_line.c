#include "fractol.h"


void	ft_mlx_line(t_env *e)
{
	t_env	c;

	c = ft_inival();
	ft_zrot(&e->xi, &e->yi, *e);
	ft_zrota(&e->xf, &e->yf, *e);
	c.d = (e->xi == e->xf) ? 100 * fabsf(e->yf - e->yi) :
	100 * fabsf(e->xf - e->xi);
	c.a = (e->xi == e->xf) ? 1 : (((float)e->yf - (float)e->yi) /
	((float)e->xf - (float)e->xi));
	c.Xi = e->xi;
	c.b = e->yi - c.a * e->xi;
	c.i = -1;
	e->color = (e->xdeg != 0) ? rand() % 999999999 : 0xFFFFFF;
	while (c.i++ <= c.d)
	{
		if (e->xi != e->xf)
		{
			c.Yi = c.a * c.Xi + c.b;
			c.Xi = (e->xi < e->xf) ? c.Xi + 0.01 : c.Xi - 0.01;
		}
		else
			c.Yi = (e->yi < e->yf) ? 0.01 * c.i + e->yi : e->yi - 0.01 * c.i;
		mlx_pixel_put_to_image(c.Xi, c.Yi, e);
	}
}
