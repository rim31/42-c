/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 09:27:37 by oseng             #+#    #+#             */
/*   Updated: 2016/01/29 17:25:33 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int    *ft_map(char const *s, char c)
{
	size_t  		a;
	size_t  		i;
	int             begin;
	int             end;
	int    			*grid;
	char				*tmp;

	tmp = NULL;
	a = 0;
	i = 0;
	grid = NULL;
	if (s && (grid = (int *)malloc(sizeof(*grid) * (ft_strlen(s) + 1))))
	{
		while (s[i] != '\0')
		{
			while (s[i] == c && s[i])
				i++;
			begin = i;
			while (s[i] != c && s[i])
				i++;
			end = i;
			if (end > begin)
				{
					tmp = ft_strsub(s, begin, (end - begin));
					grid[a] = ft_atoi(tmp);
					//ft_putnbr(grid[a]);
					a++;
				}

		}
		grid[a] = '\0';
	}
	a = 0;
	while (a < 20)
	{
		ft_putnbr(grid[a]);
		a++;
	}
	return (grid);
}
