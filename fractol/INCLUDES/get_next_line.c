/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 17:38:05 by oseng             #+#    #+#             */
/*   Updated: 2016/02/16 18:37:58 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char                     *ft_read(int fd, char *str)
{

	char                    buf[BUFF_SIZE + 1];
	int                             ret;
	char                    *tmp;
	int                             i;

	str = ft_strnew(1);
	i = 0;
	while ((ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = '\0';
		tmp = str;
		str = ft_strjoin(tmp, buf);
		ft_strdel(&tmp);
		i = 1;
	}
	if (i == 0 || ret == -1)
		return (NULL);
	return (str);
}

static char                     *ft_line(char *str)
{
	char                    *tmp;
	int                             i;

	i = 0;
	while (str[i] != '\n' && str[i] != '\0')
		i++;
	tmp = ft_strnew(i + 1);
	i = 0;
	while (str[i] != '\n' && str[i] != '\0')
	{
		tmp[i] = str[i];
		i++;
	}
	if (str[i] == '\n')
		i++;
	tmp[i] = '\0';
	return (tmp);
}

static  char            *ft_subline(char *str)
{
	char                    *tmp;
	int                             i;
	int                             j;

	i = 0;
	while (str[i] != '\n' && str[i] != '\0')
		i++;
	if (str[i] == '\n')
		i++;
	tmp = ft_strnew(ft_strlen(str) - i);
	j = 0;
	while (str[i] != '\0')
	{
		tmp[j] = str[i];
		i++;
		j++;
	}
	tmp[j] = '\0';
	str = tmp;
	return (str);
}

int                                     get_next_line(int const fd, char **line)
{
	static  char    *str[256];
	int                             i;

	i = 0;
	if (fd < 0 || line == NULL || BUFF_SIZE < 1 || fd > 256)
		return (-1);
	if (!(str[fd]))
		if ((str[fd] = ft_read(fd, str[fd])) == NULL)
			return (-1);
	if (str[fd][i] == '\0')
		return (0);
	*line = ft_line(str[fd]);
	str[fd] = ft_subline(str[fd]);
	return (1);
}
/*
int main(int argc, char const **argv)
{
	int fd;
	int a;
	char *line;
	int   ret;
	int		*grid;
	int **tab;
	int b;

	b = 0;
	if (argc > 2)
		return (0);
	fd = 0;
	if (argc == 2)
	{
		if (argv[1] == NULL)
			return (0);
		if ((fd = open(argv[1], O_RDONLY)) == -1)
			return (0);
	}
	a = 0;
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		grid = ft_map(line, ' ');
		while (grid[a])
		{
			tab[b][a] = grid[a];
			ft_putnbr(tab[b][a]);
			a++;
		}
		b++;
		ft_putstr("\n");
	}
	close (fd);
	return (0);
}
*/
