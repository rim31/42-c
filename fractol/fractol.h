# ifndef FRACTOL_H
# define FRACTOL_H
# include <fcntl.h>
# include <math.h>
#include <unistd.h>
#include "INCLUDES/libft.h"
#include "minilibx_macos/mlx.h"

typedef struct  s_env
{
	int     bpp;
	int     size_line;
	int     color;
	char    *data;
	//int     *data;
	int     endian;
	void    *mlx;
	//void **mlx; //pour avoir *mlx[1]..8 et arriver jusqu'a 8fractales
	void	*win;
	void	*image;
	int	updown;
	int	rightleft;

	float	xi;
	float	xf;
	float	yi;
	float	yf;
	int	nbcol;
	int	x;//coordonees du pixel
	int	y;
	float	z;//Nombre de Mandelbrot
	float	zi;
	float	zr;
	float	c;//constante de Mandelbrot
	float	ci;
	float	cr;
	int	i;//iteration
	int	imax;//iteration max
	//zone de dessin
	float	x1;
	float	x2;
	float	y1;
	float	y2;
	double	zoom;
	//definition de l'image
	float	img_x;
	float	img_y;
	float	tmp;
	float	width;
	float	length;
}		t_env;

int		ft_strnb(char const *s, char c);
int		my_key_funct(int keycode, t_env *a);
int		get_next_line(int const fd, char **line);
void		mlx_pixel_put_to_image(int x, int y, t_env *e);
int		my_mouse_funct(int button, int x, int y, t_env *e);
t_env		ft_mapping(t_env a);
void		ft_mandel(t_env *a);
t_env		ft_inival(void);

#endif
