#include "fractol.h"
#include <stdio.h>

//gcc fractol.c INCLUDES/libft.a -lmlx -framework OpenGl -framework AppKit

void				mlx_pixel_put_to_image(int x, int y, t_env *e)
{
/*	unsigned char	b;
	unsigned char	g;
	unsigned char	r;

	b = (e->color & 0xFF0000) >> 16;
	g = (e->color & 0xFF00) >> 8;
	r = (e->color & 0xFF);
	e->color = (e->nbcol == 0 ) ? 0xFF0000 : 0xFF00FF;
	if (y > 0 && x > 0 && x < 520 && y < 450)
	{
		e->data[y * e->size_line + x * e->bpp / 8] = r;
		e->data[y * e->size_line + x * e->bpp / 8 + 1] = g;
		e->data[y * e->size_line + x * e->bpp / 8 + 2] = b;
	}*/

	 if (y > 0 && x > 0 && x < 520 && y < 450)
	 	e->data[x + (450 * y)] = 0xFFFFFF;
}

int	my_key_funct(int keycode, t_env *a)
{
	printf("key : %d", keycode);
	if (keycode == 53)
		exit(0);
	if (keycode == 126)
		a->updown -= 10;
	if (keycode == 125)
		a->updown += 10;
	if (keycode == 123)
		a->rightleft -= 10;
	if (keycode == 124)
		a->rightleft += 10;
	if (keycode == 24)
//if (keycode == 69)
		a->imax -= 10;
	if (keycode == 44/*78*/)
		a->imax += 10;
	//ft_bzero((void *)a->data, (520 * 450));
	mlx_destroy_image(a->mlx, a->image);
	mlx_put_image_to_window(a->mlx, a->win, a->image, a->updown, a->rightleft);
	//mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
	ft_mandel(a);
	return (0);
}

int      my_mouse_funct(int button, int x, int y, t_env *e)
{
	printf("mouse : %d x : %d y : %d zoom : %f imax : %d\n", button, x, y, e->zoom, e->imax);
	if (button == 5)
	{
		// e->updown = y;
		// e->rightleft = x;
		e->zoom += 10;
		//e->imax += 10;
	}
	if (button == 4)
	{
		// e->updown = y;
		// e->rightleft = x;
		e->zoom -= 10;
		//e->imax -= 10;
	}
	if (button == 3)
	{
		if (e->color != 0)
			e->color = 0;
		else
			e->color = x * y;
	}
	//e->imax = 50 + x / 20 - y / 20;
	mlx_destroy_image(e->mlx, e->image);
	mlx_put_image_to_window(e->mlx, e->win, e->image, 0, 0);
	ft_mandel(e);
	return (0);
}

/*
 ** z0 = 0 & Zn+1 = Zn^2 + C
 ** Z = Zr +i.Zi & C = Cr + i.Ci ==> i^2 = -1
 ** devellop and factorise ==>  z = (Zr^2 - Zi^2 + Cr) + i.(2.Zr.Zi + Ci)
 ** module z < 2
 */
t_env           ft_mapping(t_env a)
{
	a.imax = 0;
	a.rightleft = 0;
	a.updown = 0;
	a.zoom = 0;
	a.mlx = mlx_init();
	a.win = mlx_new_window(a.mlx, 520, 450, "mlx 42");
	ft_mandel(&a);
	mlx_mouse_hook(a.win, my_mouse_funct, &a);
	mlx_hook(a.win, 2, 3, my_key_funct, &a); ///code Xevent Xkeypress
//	mlx_put_image_to_window(a.mlx, a.win, a.image, 0, 0);//
	mlx_loop(a.mlx);
	return (a);
}

// static t_env ft_colour(t_env a)
// {
// 	int i;
//
// 	i = 0;
// 	while (i++ < a.imax)
// 	{
// 		a.color = (0, 0, i * 255 / a.imax);
// 	}
// 	return (a);
// }

void ft_mandel(t_env *a)
{
	int x;//coordonees du pixel
	int y;
	float z;//Nombre de Mandelbrot
	float zi;
	float zr;
	float c;//constante de Mandelbrot
	float ci;
	float cr;
	int i;//iteration
	int imax;//iteration max
	//zone de dessin
	float x1;
	float x2;
	float y1;
	float y2;
	float zoom;
	//definition de l'image
	float img_x;
	float img_y;
	float tmp;


	a->image = mlx_new_image(a->mlx, a->width, a->length);
	a->data = (int *)mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
//	a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
	x1 = -2.1;
	x2 = 0.6;
	y1 = -1.2;
	y2 = 1.2;

	zoom = 200 + a->zoom;
	x = 0;
	y = 0;
	imax = 50;
	//zone de dessin : limite de Mandelbrot
	img_x = (x2 - x1) * zoom; // img_x = (0.6 +2.1)*100 = 270
	img_y = (y2 - y1) * zoom; //img_y = 240

	while (x++ < img_x)// x<270
	{
		y = 0;
		while (y++ < img_y)//y<240
		{
			cr = x / zoom + x1;// = 0/100 -2.1
			ci = y / zoom + y1;// = 0/100 -1.2
			zr = 0;
			zi = 0;
			i = 0;

			while ( zr * zr + zi * zi < 4 && i < imax + a->imax) // 0 <4 & i < 50
			{
				tmp = zr; // tmp = 0
				zr = zr * zr - zi * zi + cr; // zr = 0 - 0 - 2.1 = -2.1
				zi = 2 * tmp * zi + ci;//zi = -2*2.1 - 1.2 = -5.4
				i = i + 1;
				//printf("iteration %d\n x : %d\n y : %d\n img_x : %f\n", i, x, y, img_x);
			}
			if (i == imax)
					mlx_pixel_put(a->mlx, a->win, x + a->rightleft, y + a->updown, 0x00ff00);
			/*{
				a->color = 0x00ff00;
				mlx_pixel_put_to_image(a->x, a->y, a);
			}*/

			else
						    mlx_pixel_put(a->mlx, a->win, x + a->rightleft, y + a->updown, 0xFF00FF);
			/*{
				a->color = 0xFF00FF;
				mlx_pixel_put_to_image(a->x, a->y, a);
			}*/

			//y++;
		}
		//x++;
	}

	mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
}


int main(int argc, char **argv)
{
	t_env a;

	if (argc == 2)
	{
		if (strcmp(argv[1], "1") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "2") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "3") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "4") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "5") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "6") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "7") == 0)
			ft_mapping(a);
	}
	ft_putstr("\033[92mCHOOSE A NUMBER\033[0m\n1 : Mandelbrot\n2 : Julia\n3 : Newton\n4 : John Snow\n5 : Rubix Cube\n6 : Illuminati\n7 : Woaouw\n");
	return (0);
}
