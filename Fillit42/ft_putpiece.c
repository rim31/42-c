/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putpiece.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: baalexan <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 12:47:06 by baalexan          #+#    #+#             */
/*   Updated: 2016/01/12 12:47:08 by baalexan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		*ft_putpiece(char *sol, int a, int **save, int i, int size)
{
	sol[a] = 'A' + i;
	sol[a + save[i][0]] = 'A' + i;
	sol[a + save[i][0] + save[i][1]] = 'A' + i;
	sol[a + save[i][0] + save[i][1] + save[i][2]] ='A' + i;
	return (sol);
}
