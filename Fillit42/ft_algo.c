/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: baalexan <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 14:18:49 by baalexan          #+#    #+#             */
/*   Updated: 2016/01/14 16:39:45 by baalexan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		ft_place(char *sol, int a, int **save, int i)
{
	if (sol[a] == '.' && sol[a + save[i][0]] == '.'
			&& sol[a + save[i][0] + save[i][1]] == '.'
			&& sol[a + save[i][0] + save[i][1] + save[i][2]] == '.')
		return (1);
	return (0);
}

static int		ft_algo(char *sol, int a, int **save, int i)
{
	while (save[i][0] != -1)
	{
		while (sol[a] != '\0')
		{
			if (ft_place(sol, a, save, i) == 1)
			{
				sol[a] = 'A' + i;
				sol[a + save[i][0]] = 'A' + i;
				sol[a + save[i][0] + save[i][1]] = 'A' + i;
				sol[a + save[i][0] + save[i][1] + save[i][2]] = 'A' + i;
				if (save[i][0] == -1)
					return (1);
				else
				{
					if (ft_algo(sol, 0, save, i + 1))
						return (1);
					ft_erase(i, sol);
				}
			}
			a++;
		}
		return (0);
	}
	return (1);
}

void			ft_solv(char *sol, int **save)
{
	int x;

	x = ft_size(ft_strlen_int(save));
	while (1)
	{
		if (ft_algo(sol, 0, save, 0))
			break ;
		x++;
		sol = ft_empty_sq(x);
		save = ft_resize_one(save);
	}
	ft_putstr(sol);
	exit(1);
}
