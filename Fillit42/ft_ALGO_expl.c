/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 16:12:16 by oseng             #+#    #+#             */
/*   Updated: 2016/01/08 13:56:56 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <unistd.h>
#include <stdio.h>

/*static	int	ft_sqrt(int nb);
//fonction qui efface un tetromino lors du backtracking
void	ft_erase(char *sol, int i);
//focntion dessine un carre vide de . et \n
char	*ft_empty_sq(int nb);
char	**ft_view_bdd(char *name);
char	*ft_resize(int nbr);
static	int	i;
static	int	x;
*/

static	void	ft_plot(int *save, int i)//dessin les pieces en cours de recherche
{
	int x;
	int a;
	char *sol;
	int	table[19][3] = {{x + 5, 1, x + 4}, {x + 4, 1, x + 5}, {1, 1, x + 4},
		{x + 4, 1, 1}, {x + 5, x + 5, 1}, {1, 1, x + 3}, {1, x + 5, x + 5},
		{x + 4, 1, 1}, {x + 5, x + 5, 1} , {x + 5, 1, 1}, {1, x + 4, x + 5},
		{1, 1, x + 5}, {1, x + 4, 1}, {x + 5, x + 5, x + 5}, {1, 1, 1},
		{1, x + 5, 1}, {1, x + 3, 1}, {x + 5, 1, x + 5}, {x + 5, 1, x + 5}};

	x = 0;
	printf("%d", i);
	sol = NULL;
	if (i == 2 || i == 4 || i == 8)
		a = 3;
	else
		a = 0;
	sol = ft_empty_sq(ft_strlen_int(save) + x);
	sol[a] = 'A' + i;
	sol[a + table[i][0]] = '1' + i;
	sol[a + table[i][1] + table[i][0]] = 'A' + i;
	sol[a + table[i][2] + table[i][1] + table[i][0]]= 'A' + i;
	printf("%s", sol);
	printf("\n");
}



/*backtracking*/
int		ft_algo(char *sol, int a, int *save, int i)//a est la postion dans la solution
{
	//	int	i;
	int x;

	x = 0;
	//	char	save[] = {1, 3, 5, 6, 13, 14, '\0'};//exemle de pieces a ranger dans un carré
	int table[19][3] = {{x + 5, 1, x + 4} , {x + 4, 1, x + 5}, {1, 1, x + 4} ,
		{x + 4, 1, 1} , {x + 5, x + 5, 1} , {1, 1, x + 3} , {1, x + 5, x + 5} ,
		{x + 4, 1, 1} , {x + 5, x + 4, 1} , {x + 5, 1, 1} , {1, x + 4, x + 5} ,
		{1, 1, x + 5} , {1, x + 4, 1} , {x + 5, x + 5, x + 5} , {1, 1, 1} , 
		{1, x + 5, 1} , {1, x + 4, 1} , {x + 5, 1, x + 5} , {x + 4, 1, x + 4}};
	/*tableau avec les coordonees des pieces selon nous ^^*/
	//printf("%d \n %d", a, save[i]);//dessine le resultat
	if (sol[a] == '\0' && i == ft_strlen_int(save))// aussi a == ft_strlen(sol)) si on arrive au bout de notre gd carré, FINI
	{
		printf("%s", sol);
		return (1);
	}
	i = 0;
	while (save[i] != '\0')/*on parcours toute notre tab de sauvegarde des pieces*/
	{
	//	printf("%d \n", save[i]);//////on dessine la piece a compacter pour savoir ou on est
		if (sol[a] == '.' && sol[a + table[save[i]][0]] == '.' 
				&& sol[a + table[save[i]][0] + table[save[i]][1]] == '.'
				&& sol[a + table[save[i]][0] + table[save[i]][1] + table[save[i]][2]] =='.')
		{//on teste si la piece i rentre dans la sol, si oui, on inject la LETTRE
			sol[a] = 'A' + i;
			sol[a + table[save[i]][0]] = 'A' + i;
			sol[a + table[save[i]][0] + table[save[i]][1]] = 'A' + i;
			sol[a + table[save[i]][0] + table[save[i]][1] + table[save[i]][2]] = 'A' + i;
			if (ft_algo(sol, a + 1, save, i + 1)) //on teste la recursion avec la lettre placee dans cette case
				return (1);
		}
		i++;//on augemente la LETTRE ( posisiton dans le tableau save des pieces
	}
	if (sol[a] !=  '.' || sol[a] != '\n')//on continue a avancer dans notre carré
		return (ft_algo(sol, a + 1, save, i));
	free(sol);
	//ft_erase(sol, i);// la recursion ne fonctionne pas, on efface la piece recemment inseree
	//sol = ft_empty_sq(ft_strlen_int(save) + 1);//on agrandi le carre de 1
	ft_algo(ft_empty_sq(ft_strlen_int(save) + 1), 0, save, 0);
	return (0);//fail, on relance la recursion
}

int		main()
{
	char *sol;

	int	save[] = {13, 13, 13, 13, '\0'};
	sol = ft_empty_sq(ft_strlen_int(save));
	printf("%s \n", sol);
//	printf("%d \n", ft_algo(ft_empty_sq(ft_strlen_int(save)), 0, save, 0));
	ft_algo(ft_empty_sq(ft_strlen_int(save)), 0, save, 0);
	return (0);
}
