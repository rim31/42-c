/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putcolor.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 09:13:46 by oseng             #+#    #+#             */
/*   Updated: 2016/01/14 09:24:45 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putcolor(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == 'A' || str[i] == 'G' || str[i] == 'M' || str[i] == 'S')
			ft_putstr("\033[31m");
		else if (str[i] == 'B' || str[i] == 'H' || str[i] == 'N')
			ft_putstr("\033[32m");
		else if (str[i] == 'C' || str[i] == 'I' || str[i] == 'O')
			ft_putstr("\033[33m");
		else if (str[i] == 'D' || str[i] == 'J' || str[i] == 'P')
			ft_putstr("\033[34m");
		else if (str[i] == 'E' || str[i] == 'K' || str[i] == 'Q')
			ft_putstr("\033[35m");
		else if (str[i] == 'F' || str[i] == 'L' || str[i] == 'R')
			ft_putstr("\033[36m");
		else
			ft_putstr("\033[0m");
		ft_putchar(str[i]);
		i++;
	}
	ft_putstr("\033]0m");
}
