/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/25 15:30:58 by oseng             #+#    #+#             */
/*   Updated: 2016/03/25 18:03:02 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		ft_error(void)
{
	ft_putendl("error ");
	exit(0);
}

static int	ft_open(char *str)
{
	int fd;

	if (ft_strcmp(str, "1") == 0)
	{
		if ((fd = open("map/map1.wolf", O_RDONLY)) == -1)
			exit(0);
	}
	else if (ft_strcmp(str, "2") == 0)
	{
		if ((fd = open("map/map3.wolf", O_RDONLY)) == -1)
			exit(0);
	}
	else if (ft_strcmp(str, "3") == 0)
	{
		if ((fd = open("map/map4.wolf", O_RDONLY)) == -1)
			exit(0);
	}
	else if ((fd = open("map/map2.wolf", O_RDONLY)) == -1)
			exit(0);
	return (fd);
}

static int	ft_parse0(t_env e, char *str)
{
	int		fd;
	int		a;
	char	*line;
	int		ret;

	a = 0;
	e.x = 0;
	e.y = 0;
	fd = 0;
	// if ((fd = open("map/map5.wolf", O_RDONLY)) == -1)
	// 	exit(0);
	fd = ft_open(str);
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		if (e.y > 0 && e.x != X)
			ft_error();
		if (e.y == 0)
			e.x = ft_strlen(line);
		e.y++;
	}
	if (e.y != Y)
		ft_error();
	return (e.y);
}

t_env		ft_parse(char *str)
{
	int		fd;
	int		a;
	char	*line;
	int		ret;
	t_env	e;

	a = 0;
	e.x = 0;
	fd = 0;
	e.y = ft_parse0(e, str);
	e.map = (char**)malloc(sizeof(char*) * e.y);
	if (e.map == NULL)
		ft_error();
	// if ((fd = open("map/map5.wolf", O_RDONLY)) == -1)
	// 	exit(0);
	fd = ft_open(str);
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		e.map[a] = (char*)malloc(sizeof(char) * ft_strlen(line));
		if (e.map[a] == NULL)
			ft_error();
		e.map[a] = ft_strdup(line);
		a++;
	}
	close(fd);
	return (e);
}
