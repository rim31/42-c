#ifndef WOLF3D_H
#define WOLF3D_H
#include "../minilibx_macos/mlx.h"

typedef struct s_pixel
{
    int     im_h;
    int     im_w;
	int		bpp;
	int		size_line;
    //int     *data;
	unsigned char	*data;
	int		endian;
	void	*mlx;
	void	*win;
	void	*image;
    int     color;

}				t_pixel;


unsigned char		*mlx_pixel_put_to_img(int x, int y, t_pixel *e);
//unsigned char		*mlx_pixel_put_to_img(int color, int bpp, int size_line, int endian, int x, int y, unsigned char *data);

#endif
