#include "wolf3d.h"
//gcc main.c mlx_pixel_put.c  ./minilibx_macos/libmlx.a -framework OpenGl -framework AppKit
static unsigned char		*mlx_pixel_put_to_img1(int color, int bpp, int size_line, int endian, int x, int y, unsigned char *data)
{
	unsigned char	b;
	unsigned char	g;
	unsigned char	r;

	if (!endian)
	{
		b = (color & 0xFF0000) >> 16;
		r = (color & 0xFF);
	}
	else
	{
		r = (color & 0xFF0000) >> 16;
		b = (color & 0xFF);
	}
	g = (color & 0xFF00) >> 8;
	data[y * size_line + x * bpp / 8] = r;
	data[y * size_line + x * bpp / 8 + 1] = g;
	data[y * size_line + x * bpp / 8 + 2] = b;
	return (data);
}

int main(void)
{
    t_pixel a;
	// void *mlx;
	// void *win;
	// void *image;
	// unsigned char *data;
	// int bpp;
	// int endian;
	// int size_line;
	// int color;

	int x = 500;
	int y = 500;

    a.color = 0xFFFFF;
	a.mlx = mlx_init();
	a.win = mlx_new_window(a.mlx, x, y, "yo");
	a.image = mlx_new_image(a.mlx, x, y);
	a.image = mlx_xpm_file_to_image(a.mlx, "../hand.xpm", &x, &y);
	a.data = (unsigned char *)mlx_get_data_addr(a.image, &a.bpp, &a.size_line, &a.endian);
	// //ft_putendl((char*)data);
	// data = mlx_pixel_put_to_img1(color, bpp, size_line, endian, 50, 50, data);
    a.data = mlx_pixel_put_to_img(50, 50, &a);
	mlx_put_image_to_window(a.mlx, a.win, a.image, 250, 250);
	mlx_loop(a.mlx);
	return (0);
}

// int main(void)
// {
//     //t_pixel a;
// 	void *mlx;
// 	void *win;
// 	void *image;
// 	unsigned char *data;
// 	int bpp;
// 	int endian;
// 	int size_line;
// 	int x = 500;
// 	int y = 500;
// 	int color;
//
//     color = 0xFFFFF;
// 	mlx = mlx_init();
// 	win = mlx_new_window(mlx, x, y, "yo");
// 	image = mlx_xpm_file_to_image(mlx, "../hand.xpm", &x, &y);
// 	data = (unsigned char *)mlx_get_data_addr(image, &bpp, &size_line, &endian);
// 	//ft_putendl((char*)data);
// 	data = mlx_pixel_put_to_img1(color, bpp, size_line, endian, 50, 50, data);
//     //data = mlx_pixel_put_to_img(50, 50, &a);
// 	mlx_put_image_to_window(mlx, win, image, 250, 250);
// 	mlx_loop(mlx);
// 	return (0);
// }
