#include "wolf3d.h"

// unsigned char		*mlx_pixel_put_to_img(int color, int bpp, int size_line, int endian, int x, int y, unsigned char *data)
// {
// 	unsigned char	b;
// 	unsigned char	g;
// 	unsigned char	r;
//
// 	if (!endian)
// 	{
// 		b = (color & 0xFF0000) >> 16;
// 		r = (color & 0xFF);
// 	}
// 	else
// 	{
// 		r = (color & 0xFF0000) >> 16;
// 		b = (color & 0xFF);
// 	}
// 	g = (color & 0xFF00) >> 8;
// 	data[y * size_line + x * bpp / 8] = r;
// 	data[y * size_line + x * bpp / 8 + 1] = g;
// 	data[y * size_line + x * bpp / 8 + 2] = b;
// 	return (data);
// }

unsigned char		*mlx_pixel_put_to_img(int x, int y, t_pixel *e)
{
	unsigned char	b;
	unsigned char	g;
	unsigned char	r;

	if (!e->endian)
	{
		b = (e->color & 0xFF0000) >> 16;
		r = (e->color & 0xFF);
	}
	else
	{
		r = (e->color & 0xFF0000) >> 16;
		b = (e->color & 0xFF);
	}
	g = (e->color & 0xFF00) >> 8;
	e->data[y * e->size_line + x * e->bpp / 8] = r;
	e->data[y * e->size_line + x * e->bpp / 8 + 1] = g;
	e->data[y * e->size_line + x * e->bpp / 8 + 2] = b;
	return (e->data);
}
