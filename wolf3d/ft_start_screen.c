/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_start_screen.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 18:42:23 by oseng             #+#    #+#             */
/*   Updated: 2016/03/25 18:03:15 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	ft_start_screen(t_env a)
{
	if (a.map[(int)(a.posx)][(int)(a.posy)] != '0' || a.posx > X ||
	 a.posx < 0 || a.posy > Y || a.posy < 0)
		ft_error();
	a.mlx = mlx_init();
	a.win = mlx_new_window(a.mlx, a.width, a.higth, "mlx 42");
	a.image = mlx_new_image(a.mlx, a.width, a.higth);
	if (!(a.sky = mlx_xpm_file_to_image(a.mlx, "img/sky.xpm", &a.pxlh2,
	 &a.pxlw2)))
		ft_error();
	mlx_mouse_hook(a.win, my_mouse_funct, &a);
	mlx_hook(a.win, 6, 0, my_mouse_fct, &a);
	mlx_hook(a.win, 2, (1L << 0), my_key_funct, &a);
	mlx_hook(a.win, 3, (1L << 1), my_keyrelease_funct, &a);
	mlx_loop_hook(a.mlx, ft_move, &a);
	mlx_loop(a.mlx);
}
