#include "fractol.h"
#include <stdio.h>

t_env ft_inival(void)
{
	t_env a;



	int     bpp;
	int     size_line;
	int     color;
	char    *data;
	int     endian;
	void    *win;
	void    *image;

	a.updown = 0;
	a.rightleft = 0;
	a.xi = 0;
	a.xf = 0;
	a.yi = 0;
	a.yf = 0;
	a.zoom = 0;
	a.color = 0x00FFFFFF;

	a.x = 0;//coordonees du pixel
	a.y = 0;
	a.z = 0;//Nombre de Mandelbrot
	a.zi = 0;
	a.zr = 0;
	a.c = 0;//constante de Mandelbrot
	a.ci = 0;
	a.cr = 0;
	a.i = 0;//iteration
	a.imax = 50;//iteration max
	//zone de dessin
	a.zoom = 100;
	//zone de dessin : limite de Mandelbrot
	a.x1 = -2.1;
	a.x2 = 0.6;
	a.y1 = -1.2;
	a.y2 = 1.2;
	//definition de l'image
	a.img_x = 0;
	a.img_y = 0;
	a.tmp = 0;
	a.width = 270;
	a.length = 240;
	return (a);
}

int      my_mouse_funct(int button, int x, int y, t_env *e)
{
	printf("mouse : %d\n", button);
	if (button == 5)
		e->zoom += 10;
	if (button == 4)
		e->zoom -= 10;
	if (button == 3)
	{
		if (e->color != 0)
			e->color = 0;
		else
			e->color = x * y;
	}
	mlx_destroy_image(e->mlx, e->image);
	mlx_put_image_to_window(e->mlx, e->win, e->image, 0, 0);
	ft_mandel(e);
	return (0);
}

/*
 ** z0 = 0 & Zn+1 = Zn^2 + C
 ** Z = Zr +i.Zi & C = Cr + i.Ci ==> i^2 = -1
 ** devellop and factorise ==>  z = (Zr^2 - Zi^2 + Cr) + i.(2.Zr.Zi + Ci)
 ** module z < 2
 */
t_env           ft_mapping(t_env a)
{
  a.updown = 0;
  a.rightleft = 0;
	a.mlx = mlx_init();
	a.win = mlx_new_window(a.mlx, a.width, a.length, "mlx 42");
	ft_mandel(&a);
	mlx_mouse_hook(a.win, my_mouse_funct, &a);
	mlx_hook(a.win, 2, 3, my_key_funct, &a); ///code Xevent Xkeypress
	mlx_loop(a.mlx);
	return (a);
}

// static t_env ft_colour(t_env a)
// {
// 	int i;
//
// 	i = 0;
// 	while (i++ < a.imax)
// 	{
// 		a.color = (0, 0, i * 255 / a.imax);
// 	}
// 	return (a);
// }

void ft_mandel(t_env *a)
{
	// float x;//coordonees du pixel
	// float y;
	// float z;//Nombre de Mandelbrot
	// float zi;
	// float zr;
	// float c;//constante de Mandelbrot
	// float ci;
	// float cr;
	// float i;//iteration
	// float imax;//iteration max
	// //zone de dessin
	// float x1;
	// float x2;
	// float y1;
	// float y2;
	// float zoom;
	// //definition de l'image
	// float img_x;
	// float img_y;
	// float tmp;


	a->image = mlx_new_image(a->mlx, a->width, a->length);
	a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);

	a->z = 0;
	a->zi = 0;
	a->zr = 0;
	a->i = 0;
	a->imax = 50;
	a->zoom = 100;
	//zone de dessin : limite de Mandelbrot
	a->x1 = -2.1;
	a->x2 = 0.6;
	a->y1 = -1.2;
	a->y2 = 1.2;
	a->img_x = (a->x2 - a->x1) * a->zoom;
	a->img_y = (a->y2 - a->y1) * a->zoom;

	while (a->y < a->img_y)
	{
    a->y = 0;
		while (a->x < a->img_x)
		{
			a->cr = a->x / a->zoom + a->x1;
			a->ci = a->y / a->zoom + a->y1;
			a->zr = 0;
			a->zi = 0;
			a->i = 0;

			while ( a->zr * a->zr + a->zi * a->zi < 4 && a->i < a->imax)
			{
				a->tmp = a->zr;
				a->zr = a->zr * a->zr - a->zi * a->zi + a->cr;
				a->zi = 2 * a->tmp * a->zi + a->ci;
				a->i = a->i + 1;
			//	printf("iteration %d\n x : %f\n y : %f\n", a->i, a->x, a->y);
			}
			if (a->i == a->imax)
				//mlx_pixel_put_to_image(a->x, a->y , a->mlx);
				mlx_pixel_put(a->mlx, a->win, a->x, a->y, 0x0000ff00);
			//mlx_pixel_put_to_image(a->x, a->y, a);
			else
				//mlx_pixel_put_to_image(a->x, a->y, a->mlx);
				//mlx_pixel_put_to_image(a->x, a->y, a);
				mlx_pixel_put(a->mlx, a->win, a->x, a->y, 0x00FF00FF);
			//mlx_string_put ( a->mlx, a->win, a->x, a->y, a->color, "test");
			a->x++;
		}
		a->y++;
	}
ft_putstr("fini");
}


int main(int argc, char **argv)
{
	t_env a;

	a = ft_inival();
	if (argc == 2)
	{
		if (strcmp(argv[1], "1") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "2") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "3") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "4") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "5") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "6") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "7") == 0)
			ft_mapping(a);
	}
	ft_putstr("\033[92mCHOOSE A NUMBER\033[0m\n1 : Mandelbrot\n2 : Julia\n3 : Newton\n4 : John Snow\n5 : Rubix Cube\n6 : Illuminati\n7 : Woaouw\n");
	return (0);
}
