#include "fractol.h"
#include <stdio.h>

//gcc fractol.c INCLUDES/libft.a -lmlx -framework OpenGl -framework AppKit

void				mlx_pixel_put_to_image(int x, int y, t_env *e)
{
	unsigned char	b;
	unsigned char	g;
	unsigned char	r;

	b = (e->color & 0xFF0000) >> 16;
	g = (e->color & 0xFF00) >> 8;
	r = (e->color & 0xFF);
	//e->color = (e->nbcol == 0 ) ? rand() % 999999999 : 0xFF00FF;
	if (y > 0 && x > 0 && x < 520 && y < 450)
	{
		e->data[y * e->size_line + x * e->bpp / 8] = r;
		e->data[y * e->size_line + x * e->bpp / 8 + 1] = g;
		e->data[y * e->size_line + x * e->bpp / 8 + 2] = b;
	}


	// define RGB(r, g, b)(256 * 256 * (int)(r) + 256 * (int)(g) + (int)(b))
	// int couleur(double t)
	// {
	// return ((RGB(
	// 127.5 * (cos(t) + 1),
	// 127.5 * (sin(t) + 1),
	// 127.5 * (1 - cos(t))
	// ));
	// }


	// if (y > 0 && x > 0 && x < 520 && y < 450)
	// 	{
	// 	if (e->nbcol == 0)
	// 		e->data[x + (450 * y)] = 0xFF0000;
	// 	else
	// 		e->data[x + (450 * y)] = 0xFF00FF;
	// 	}
}

int	my_key_funct(int keycode, t_env *a)
{
	if (keycode == 53)
		exit(0);
	if (keycode == 126)
		a->updown -= 0.1;
	if (keycode == 125)
		a->updown += 0.1;
	if (keycode == 123)
		a->rightleft -= 0.1;
	if (keycode == 124)
		a->rightleft += 0.1;
	if (keycode == 69)
		a->iimax -= 10;
	if (keycode == 78)
		a->iimax += 10;
	ft_bzero((void *)a->data, (a->length * a->width));
	//mlx_destroy_image(a->mlx, a->image);
//	mlx_clear_window(a->mlx, a->win);
	//mlx_put_image_to_window(a->mlx, a->win, a->image, a->updown, a->rightleft);
	//mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
	ft_mandel(a);
	return (0);
}

int      my_mouse_funct(int button, int x, int y, t_env *e)
{
//	printf("mouse : %d x : %d y : %d zoom : %f iteration iter : %d\n", button, x, y, e->zoom, e->imax);
	if (button == 5)
		{
			// e->updown = e->width / 2 - y;
			// e->rightleft = e->length / 2 - x;
			e->zooom += 10;
			//e->imax += 10;
		}
	if (button == 4)
	{
		// e->updown = e->width / 2 - y;
		// e->rightleft = e->length / 2 - x;
		e->zooom -= 10;
		//e->imax -= 10;
	}
	if (button == 3)
	{
		if (e->nbcol != 0)
			e->nbcol = 0;
		else
			e->nbcol = x * y;
	}
		ft_bzero((void *)e->data, (e->length * e->width));
//e->imax = 50 + x / 20 - y / 20;
	//mlx_destroy_image(e->mlx, e->image);
	//mlx_clear_window(e->mlx, e->win);
	mlx_put_image_to_window(e->mlx, e->win, e->image, 0, 0);
	ft_mandel(e);
	return (0);
}

/*
 ** z0 = 0 & Zn+1 = Zn^2 + C
 ** Z = Zr +i.Zi & C = Cr + i.Ci ==> i^2 = -1
 ** devellop and factorise ==>  z = (Zr^2 - Zi^2 + Cr) + i.(2.Zr.Zi + Ci)
 ** module z < 2
 */
t_env           ft_mapping(t_env a)
{
	a.x1 = -2.1;
	a.x2 = 0.6;
	a.y1 = -1.2;
	a.y2 = 1.2;
	a.imax = 50;
	a.iimax = 0;

	a.zoom = 200;
	a.zooom = 0;
	a.x = 0;
	a.y = 0;
	a.nbcol = 0;

	a.width = 520;
	a.length = 450;

	a.imax = 50;
	a.rightleft = 0;
	a.updown = 0;
	a.zoom = 0;
	a.mlx = mlx_init();
	a.win = mlx_new_window(a.mlx, a.width, a.length, "mlx 42");
	ft_mandel(&a);
	mlx_string_put(a.mlx, a.win, 10, 10, 0xffffff, "Mandelbrot");
	mlx_mouse_hook(a.win, my_mouse_funct, &a);
	mlx_hook(a.win, 2, 3, my_key_funct, &a); ///code Xevent Xkeypress
//	mlx_put_image_to_window(a.mlx, a.win, a.image, 0, 0);
	mlx_loop(a.mlx);
	return (a);
}

// static t_env ft_colour(t_env a)
// {
// 	int i;
//
// 	i = 0;
// 	while (i++ < a.imax)
// 	{
// 		a.color = (0, 0, i * 255 / a.imax);
// 	}
// 	return (a);
// }


void ft_mandel(t_env *a)
{
	// int x;//coordonees du pixel
	// int y;
	// float z;//Nombre de Mandelbrot
	// float zi;
	// float zr;
	// float c;//constante de Mandelbrot
	// float ci;
	// float cr;
	// int i;//iteration
	// int imax;//iteration max
	// //zone de dessin
	// float x1;
	// float x2;
	// float y1;
	// float y2;
	// float zoom;
	// //definition de l'image
	// float img_x;
	// float img_y;
	// float tmp;


	a->image = mlx_new_image(a->mlx, a->width, a->length);
	//a->data = (int *)mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
  a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
  a->x1 = -2.1 - a->rightleft;
	a->x2 = 0.6 - a->rightleft;
	a->y1 = -1.2 - a->updown;
	a->y2 = 1.2 - a->updown;
	a->imax = 50;

	a->zoom = 200 + a->zooom;
  a->x = 0;
  a->y = 0;
	//zone de dessin : limite de Mandelbrot
	a->img_x = (a->x2 - a->x1) * a->zoom; // img_x = (0.6 +2.1)*100 = 270
	a->img_y = (a->y2 - a->y1) * a->zoom; //img_y = 240

	while (a->x++ < a->img_x)// x<270
	{
    a->y = 0;
		while (a->y++ < a->img_y)//y<240
		{
			a->cr = a->x / a->zoom + a->x1;// = 0/100 -2.1
			a->ci = a->y / a->zoom + a->y1;// = 0/100 -1.2
			a->zr = 0;
			a->zi = 0;
			a->i = 0;

			while ( a->zr * a->zr + a->zi * a->zi < 4 && a->i < (a->imax + a->iimax)) // 0 <4 & i < 50
			{
				a->tmp = a->zr; // tmp = 0
				a->zr = a->zr * a->zr - a->zi * a->zi + a->cr; // zr = 0 - 0 - 2.1 = -2.1
				a->zi = 2 * a->tmp * a->zi + a->ci;//zi = -2*2.1 - 1.2 = -5.4
				a->i = a->i + 1;
				//printf("iteration %d\n x : %d\n y : %d\n img_x : %f\n", i, x, y, img_x);
			}
			if (a->i == a->imax)
		//			mlx_pixel_put(a->mlx, a->win, a->x + a->rightleft, a->y + a->updown, 0x00ff00);
				{
					a->nbcol = 0;
					//a->color = rand() % 999999999;
					//a->color = 0x00ff00;
				  a->color = 153 * (a->i / a->imax);
					//a->color = 0xFF00FF;
          // mlx_pixel_put_to_image(a->x + a->rightleft, a->y + a->updown, a);
					  mlx_pixel_put_to_image(a->x, a->y, a);
				}

			else
		//	    mlx_pixel_put(a->mlx, a->win, a->x + a->rightleft, a->y + a->updown, 0xFF00FF);
				{
						a->nbcol = 1;
          	a->color = 255 * 255 * (a->i / a->imax) + 255*255 + 255; //bleu noir
           //	mlx_pixel_put_to_image(a->x + a->rightleft, a->y + a->updown, a);
					 mlx_pixel_put_to_image(a->x, a->y, a);
				}

			//y++;
		}
		//x++;
	}

	mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
}


int main(int argc, char **argv)
{
	t_env a;

	if (argc == 2)
	{
		if (strcmp(argv[1], "1") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "2") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "3") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "4") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "5") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "6") == 0)
			ft_mapping(a);
		else if (strcmp(argv[1], "7") == 0)
			ft_mapping(a);
	}
	ft_putstr("\033[92mCHOOSE A NUMBER\033[0m\n1 : Mandelbrot\n2 : Julia\n3 : Newton\n4 : John Snow\n5 : Rubix Cube\n6 : Illuminati\n7 : Woaouw\n");
	return (0);
}
