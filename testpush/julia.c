/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oseng <oseng@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/20 16:58:26 by oseng             #+#    #+#             */
/*   Updated: 2016/02/25 20:02:02 by oseng            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <complex.h>

/*
**gcc julia.c INCLUDES/libft.a -lmlx -framework OpenGl -framework AppKit -o julia
*/

void				mlx_pixel_put_to_image(int x, int y, t_env *e)
{
	unsigned char	b;
	unsigned char	g;
	unsigned char	r;

	b = (e->color & 0xFF0000) >> 16;
	g = (e->color & 0xFF00) >> 8;
	r = (e->color & 0xFF);
	if (y > 0 && x > 0 && x < e->width && y < e->length)
	{
		e->data[y * e->size_line + x * e->bpp / 8] = r;
		e->data[y * e->size_line + x * e->bpp / 8 + 1] = g;
		e->data[y * e->size_line + x * e->bpp / 8 + 2] = b;
	}
}

static void ft_new(int i)
{
	ft_mapping(ft_itoa(i));
}


static void ft_menu(t_env a, int nb)
{
	if (nb == 91)
			ft_new(8);
	if (nb >= 83 && nb <= 89)
		ft_new(nb - 82);
 mlx_string_put(a.mlx, a.win, 1, 1, 0xff00ff, "Menu  Key");
 mlx_string_put(a.mlx, a.win, 10, 15, 0x009999, "move : <- ^ v ->");
 mlx_string_put(a.mlx, a.win, 10, 30, 0x999999, "zoom : page up or down");
 mlx_string_put(a.mlx, a.win, 10, 45, 0x999999, "iteration : + or -");
 mlx_string_put(a.mlx, a.win, 10, 15, 0x009999, "          | |");
 mlx_string_put(a.mlx, a.win, 10, 60, 0x999999, "fractal : 1 2 3 4 5 6 7 8 ");
 mlx_pixel_put_to_image(a.x, a.y, &a);
 mlx_hook(a.win, 2, 3, my_key_funct, &a);
 mlx_loop(a.mlx);
}

int					my_key_funct(int keycode, t_env *a)
{
	if (keycode == 53)
		exit(0);
	if (keycode == 126)
		a->updown -= 0.1 / a->zooom;
	if (keycode == 125)
		a->updown += 0.1 / a->zooom;
	if (keycode == 123)
		a->rightleft -= 0.1 / a->zooom;
	if (keycode == 124)
		a->rightleft += 0.1 / a->zooom;
	if (keycode == 78 && a->imax > 14)
		a->iimax -= 14;
	if (keycode == 69)
		a->iimax += 14;
	if (keycode == 121 && a->zooom > 1.1)
		a->zooom /= 1.10;
	if (keycode == 116)
		a->zooom *= 1.10;
	if (keycode == 49 || (keycode >= 83 && keycode <= 92))
		ft_menu(*a, keycode);
	ft_bzero((void *)a->data, (a->width * a->length));
	ft_fct(a);
	return (0);
}

/*
**e->iimax = ((x - e->width / 2)/ 10) + ((y + e->length / 2)/ 10) + 50;
*/

int					my_mouse_fct(int x, int y, t_env *e)
{
	if (x > 0 && x < e->width && y > 0 && y < e->length)
	{
		e->paramx = (float)((x - e->width / 2) / e->width);
		e->paramy = (float)((y - e->length / 2) / e->length);
		ft_bzero((void *)e->data, (e->width * e->length));
		ft_fct(e);
	}
	return (0);
}

/*
**zoom = y / largeur * largeur fe la fenetre * %zoom + y1 * y/largeur
*/


int					my_mouse_funct(int button, int x, int y, t_env *e)
{
	if (button == 5)
	{
		e->zooom *= 1.10;
		e->updown = (y / e->length) * (e->x1 - e->x2 + e->rightleft)
			* e->zooom / (e->zoom + e->zooom) + e->y1 * (y / e->length);
		e->rightleft = (x / e->width) * (e->y1 - e->y2 + e->updown) *
			e->zooom / (e->zoom + e->zooom) + e->x1 * (x / e->width);
	}
	if (button == 4 && e->zooom > 1)
	{
		e->zooom /= 1.10;
		e->updown = (y / e->length) * (e->x1 - e->x2 + e->rightleft) *
			e->zooom / (e->zoom + e->zooom) + e->y1 * (y / e->length);
		e->rightleft = (x / e->width) * (e->y1 - e->y2 + e->updown) *
			e->zooom / (e->zoom + e->zooom) + e->x1 * (x / e->width);
	}
	if (button == 3)
		e->nbcol = (e->nbcol != 0) ? 0 : 1;
	ft_bzero((void *)e->data, (e->width * e->length));
	ft_fct(e);
	return (0);
}

static t_env		ft_init_julia(void)
{
	t_env			a;

	a.x1 = -1;
	a.x2 = 1;
	a.y1 = -1.2;
	a.y2 = 1.2;
	a.imax = 150;
	a.iimax = 0;
	a.zoom = 100;
	a.zooom = 1;
	a.x = 0;
	a.y = 0;
	a.nbcol = 0;
	a.width = 400;
	a.length = 490;
	a.imax = 50;
	a.rightleft = 0;
	a.updown = 0;
	a.zoom = 0;
	a.paramx = 0;
	a.paramy = 0;
	return (a);
}

/*
** z0 = 0 & Zn+1 = Zn^2 + C
** Z = Zr +i.Zi & C = Cr + i.Ci ==> i^2 = -1
** devellop and factorise ==>  z = (Zr^2 - Zi^2 + Cr) + i.(2.Zr.Zi + Ci)
** module z < 2
*/

t_env				ft_mapping(char *str)
{
	t_env			a;

	if (ft_atoi(str) == 2)
		a = ft_init_julia();
	else if (ft_atoi(str) == 1)
		a = ft_init_mandel();
	else if (ft_atoi(str) == 3)
		a = ft_init_ship();
	else if (ft_atoi(str) == 4)
		a = ft_init_bunny();
	else if (ft_atoi(str) == 5)
		a = ft_init_dendrite();
	else if (ft_atoi(str) == 6)
		a = ft_init_heart();
	else if (ft_atoi(str) == 7)
		a = ft_init_ruby();
	else if (ft_atoi(str) == 8)
			a = ft_init_celt();
	a.mlx = mlx_init();
	a.nbfract = ft_atoi(str);
	a.win = mlx_new_window(a.mlx, a.width, a.length, "mlx 42");
	ft_fct(&a);
	mlx_mouse_hook(a.win, my_mouse_funct, &a);
	mlx_hook(a.win, 6, 0, my_mouse_fct, &a);
	mlx_hook(a.win, 2, 3, my_key_funct, &a);
	mlx_loop(a.mlx);
	return (a);
}

/*
**	//a->data =
**(int *)mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
*/

void			ft_plot(t_env *a)
{
	if (a->i == a->imax + a->iimax)
	{
		if (a->nbcol == 0)
			a->color = 0x000000;
		else
			a->color = (a->i * a->color1 / (a->imax + a->iimax));
		mlx_pixel_put_to_image(a->x, a->y, a);
	}
	else
	{
		if (a->nbcol == 0)
			a->color = (a->i * 255 * 255 / (a->imax + a->iimax));
		else
			a->color = (a->i * a->color1 / (a->imax + a->iimax));
		mlx_pixel_put_to_image(a->x, a->y, a);
	}
}


static void			ft_init_julia1(t_env *a)
{
	mlx_string_put(a->mlx, a->win, 10, 10, 0xffffff, "Julia");
	a->image = mlx_new_image(a->mlx, a->width, a->length);
	a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
	a->x1 = -1 - a->rightleft;
	a->x2 = 1 - a->rightleft;
	a->y1 = -1.2 - a->updown;
	a->y2 = 1.2 - a->updown;
	a->zoom = 200 * a->zooom;
	a->img_x = (a->x2 - a->x1) * a->zoom * a->zooom;
	a->img_y = (a->y2 - a->y1) * a->zoom * a->zooom;
	if (a->nbcol != 0)
	{
		a->color0 = rand() % 999999999;
		a->color = rand() % 999999999;
		a->color1 = rand() % 999999999;
	}
}

void				ft_julia(t_env *a)
{
	ft_init_julia1(a);
	a->x = 0;
	while (a->x++ < a->img_x && a->x <= a->width)
	{
		a->y = 0;
		while (a->y++ < a->img_y && a->y <= a->length)
		{
			a->zr = a->x / a->zoom + a->x1;
			a->zi = a->y / a->zoom + a->y1;
			a->cr = 0.285 + a->paramx;
			a->ci = 0.01 + a->paramy;
			a->i = -1;
			while (a->zr * a->zr + a->zi * a->zi < 4
					&& a->i++ < (a->imax + a->iimax))
			{
				a->tmp = a->zr;
				a->zr = a->zr * a->zr - a->zi * a->zi + a->cr;
				a->zi = 2 * a->tmp * a->zi + a->ci;
			}
			ft_plot(a);
		}
	}
	mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
}

/*
**C
*/


t_env				ft_init_mandel(void)
{
	t_env a;

	a.x1 = -2.1;
	a.x2 = 0.6;
	a.y1 = -1.2;
	a.y2 = 1.2;
	a.imax = 50;
	a.iimax = 0;
	a.zoom = 200;
	a.zooom = 1;
	a.x = 0;
	a.y = 0;
	a.nbcol = 0;
	a.width = 520;
	a.length = 450;
	a.imax = 50;
	a.rightleft = 0;
	a.updown = 0;
	a.zoom = 0;
	a.param = 0;
	return (a);
}

void				ft_init_mandel1(t_env *a)
{
	a->image = mlx_new_image(a->mlx, a->width, a->length);
	a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
	mlx_string_put(a->mlx, a->win, 10, 10, 0xffffff, "Mandel");
	a->x1 = -2.1 - a->rightleft;
	a->x2 = 0.6 - a->rightleft;
	a->y1 = -1.2 - a->updown;
	a->y2 = 1.2 - a->updown;
	a->zoom = 200 * a->zooom;
	a->img_x = (a->x2 - a->x1) * a->zoom * a->zooom;
	a->img_y = (a->y2 - a->y1) * a->zoom * a->zooom;
	if (a->nbcol != 0)
	{
		a->color0 = rand() % 999999999;
		a->color = rand() % 999999999;
		a->color1 = rand() % 999999999;
	}
}

void				ft_mandel(t_env *a)
{
	ft_init_mandel1(a);
	a->x = 0;
	while (a->x++ < a->img_x && a->x <= a->width)
	{
		a->y = 0;
		while (a->y++ < a->img_y && a->y <= a->length)
		{
			a->cr = a->x / a->zoom + a->x1;
			a->ci = a->y / a->zoom + a->y1;
			a->zr = 0 + a->paramx;
			a->zi = 0 + a->paramy;
			a->i = -1;
			while (a->zr * a->zr + a->zi * a->zi < 4
					&& a->i++ < (a->imax + a->iimax))
			{
				a->tmp = a->zr;
				a->zr = a->zr * a->zr - a->zi * a->zi + a->cr;
				a->zi = 2 * a->tmp * a->zi + a->ci;
			}
			ft_plot(a);
		}
	}
	mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
}

t_env				ft_init_ship(void)
{
	t_env a;

	a.x1 = -2.1;
	a.x2 = 0.6;
	a.y1 = -1.2;
	a.y2 = 1.2;
	a.imax = 50;
	a.iimax = 0;
	a.zoom = 200;
	a.zooom = 1;
	a.x = 0;
	a.y = 0;
	a.nbcol = 0;
	a.width = 520;
	a.length = 480;
	a.imax = 50;
	a.rightleft = 0;
	a.updown = .7;
	a.zoom = 0;
	a.param = 0;
	return (a);
}

void				ft_init_ship1(t_env *a)
{
	a->image = mlx_new_image(a->mlx, a->width, a->length);
	a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
	mlx_string_put(a->mlx, a->win, 10, 10, 0xffffff, "Mandel");
	a->x1 = -2.1 - a->rightleft;
	a->x2 = 0.6 - a->rightleft;
	a->y1 = -1.2 - a->updown;
	a->y2 = 1.2 - a->updown;
	a->zoom = 200 * a->zooom;
	a->img_x = (a->x2 - a->x1) * a->zoom * a->zooom;
	a->img_y = (a->y2 - a->y1) * a->zoom * a->zooom;
	if (a->nbcol != 0)
	{
		a->color0 = rand() % 999999999;
		a->color = rand() % 999999999;
		a->color1 = rand() % 999999999;
	}
}

void				ft_ship(t_env *a)
{
	ft_init_ship1(a);
	a->x = 0;
	while (a->x++ < a->img_x && a->x <= a->width)
	{
		a->y = 0;
		while (a->y++ < a->img_y && a->y <= a->length)
		{
			a->cr = a->x / a->zoom + a->x1;
			a->ci = a->y / a->zoom + a->y1;
			a->zr = 0 + a->paramx;
			a->zi = 0 + a->paramy;
			a->i = -1;
			while (a->zr * a->zr + a->zi * a->zi < 4
					&& a->i++ < (a->imax + a->iimax))
			{
				a->tmp = a->zr;
				a->zr = fabs(a->zr) * fabs(a->zr) - fabs(a->zi) * fabs(a->zi) + a->cr;
				a->zi = 2 * fabs(a->tmp) * fabs(a->zi) + a->ci;
			}
			ft_plot(a);
		}
	}
	mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
}

t_env				ft_init_bunny(void)
{
	t_env a;

	a.x1 = -1;
	a.x2 = 1;
	a.y1 = -1.2;
	a.y2 = 1.2;
	a.imax = 50;
	a.iimax = 0;
	a.zoom = 200;
	a.zooom = 1;
	a.x = 0;
	a.y = 0;
	a.nbcol = 0;
	a.width = 540;
	a.length = 480;
	a.imax = 50;
	a.rightleft = -0.8;
	a.updown = 0;
	a.zoom = 0;
	a.param = 0;
	return (a);
}

void				ft_init_bunny1(t_env *a)
{
	a->image = mlx_new_image(a->mlx, a->width, a->length);
	a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
	a->x1 = -2.1 - a->rightleft;
	a->x2 = 0.6 - a->rightleft;
	a->y1 = -1.2 - a->updown;
	a->y2 = 1.2 - a->updown;
	a->zoom = 200 * a->zooom;
	a->img_x = (a->x2 - a->x1) * a->zoom * a->zooom;
	a->img_y = (a->y2 - a->y1) * a->zoom * a->zooom;
	if (a->nbcol != 0)
	{
		a->color0 = rand() % 999999999;
		a->color = rand() % 999999999;
		a->color1 = rand() % 999999999;
	}
}

void				ft_bunny(t_env *a)
{
	ft_init_bunny1(a);
	a->x = 0;
	while (a->x++ < a->img_x && a->x <= a->width)
	{
		a->y = 0;
		while (a->y++ < a->img_y && a->y <= a->length)
		{
			a->zr = a->x / a->zoom + a->x1;
			a->zi = a->y / a->zoom + a->y1;
			a->cr = -0.123 + a->paramx;
			a->ci = 0.745 + a->paramy;
			a->i = -1;
			while (a->zr * a->zr + a->zi * a->zi < 4
					&& a->i++ < (a->imax + a->iimax))
			{
				a->tmp = a->zr;
				a->zr = a->zr * a->zr - a->zi * a->zi + a->cr;
				a->zi = 2 * a->tmp * a->zi + a->ci;
			}
			ft_plot(a);
		}
	}
	mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
}

t_env				ft_init_dendrite(void)
{
	t_env a;

	a.x1 = -1;
	a.x2 = 1;
	a.y1 = -1.2;
	a.y2 = 1.2;
	a.imax = 50;
	a.iimax = 0;
	a.zoom = 200;
	a.zooom = 1;
	a.x = 0;
	a.y = 0;
	a.nbcol = 0;
	a.width = 540;
	a.length = 480;
	a.imax = 50;
	a.rightleft = -0.8;
	a.updown = 0;
	a.zoom = 0;
	a.param = 0;
	return (a);
}

void				ft_init_dendrite1(t_env *a)
{
	a->image = mlx_new_image(a->mlx, a->width, a->length);
	a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
	a->x1 = -2.1 - a->rightleft;
	a->x2 = 0.6 - a->rightleft;
	a->y1 = -1.2 - a->updown;
	a->y2 = 1.2 - a->updown;
	a->zoom = 200 * a->zooom;
	a->img_x = (a->x2 - a->x1) * a->zoom * a->zooom;
	a->img_y = (a->y2 - a->y1) * a->zoom * a->zooom;
	if (a->nbcol != 0)
	{
		a->color0 = rand() % 999999999;
		a->color = rand() % 999999999;
		a->color1 = rand() % 999999999;
	}
}

void				ft_dendrite(t_env *a)
{
	ft_init_dendrite1(a);
	a->x = 0;
	while (a->x++ < a->img_x && a->x <= a->width)
	{
		a->y = 0;
		while (a->y++ < a->img_y && a->y <= a->length)
		{
			a->zr = a->x / a->zoom + a->x1;
			a->zi = a->y / a->zoom + a->y1;
			a->cr = 0 + a->paramx;
			a->ci = 1 + a->paramy;
			a->i = -1;
			while (a->zr * a->zr + a->zi * a->zi  < 4
					&& a->i++ < (a->imax + a->iimax))
			{
				a->tmp = a->zr;
				a->zr = a->zr * a->zr - a->zi * a->zi + a->cr;
				a->zi = 2 * a->tmp * a->zi + a->ci;
			}
			ft_plot(a);
		}
	}
	mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
}


t_env				ft_init_heart(void)
{
	t_env a;

	a.x1 = -2.5;
	a.x2 = 2.5;
	a.y1 = -1.2;
	a.y2 = 1.2;
	a.imax = 50;
	a.iimax = 0;
	a.zoom = 200;
	a.zooom = 1;
	a.x = 0;
	a.y = 0;
	a.nbcol = 0;
	a.width = 500;
	a.length = 480;
	a.imax = 50;
	a.rightleft = -0.6;
	a.updown = -0.3;
	a.zoom = 0;
	a.param = 0;
	return (a);
}

void				ft_init_heart1(t_env *a)
{
	a->image = mlx_new_image(a->mlx, a->width, a->length);
	a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
	a->x1 = -2.1 - a->rightleft;
	a->x2 = 0.6 - a->rightleft;
	a->y1 = -1.2 - a->updown;
	a->y2 = 1.2 - a->updown;
	a->zoom = 200 * a->zooom;
	a->img_x = (a->x2 - a->x1) * a->zoom * a->zooom;
	a->img_y = (a->y2 - a->y1) * a->zoom * a->zooom;
	if (a->nbcol != 0)
	{
		a->color0 = rand() % 999999999;
		a->color = rand() % 999999999;
		a->color1 = rand() % 999999999;
	}
}

void				ft_heart(t_env *a)
{
	ft_init_heart1(a);
	a->x = 0;
	while (a->x++ < a->img_x && a->x <= a->width)
	{
		a->y = 0;
		while (a->y++ < a->img_y && a->y <= a->length)
		{
			a->cr = a->x / a->zoom + a->x1;
			a->ci = a->y / a->zoom + a->y1;
			a->zr = 0 + a->paramx;
			a->zi = 0 + a->paramy;
			a->i = -1;
			while (a->zr * a->zr + a->zi * a->zi  < 4
					&& a->i++ < (a->imax + a->iimax))
			{
				a->tmp = a->zr;
				a->zr = (a->zr * a->zr - (a->zi * a->zi * 3)) * fabs(a->zr) + a->cr;
				a->zi = ((a->zr * a->zr * 3) - a->zi * a->zi) * fabs(a->zi) + a->ci;
			}
			ft_plot(a);
		}
	}
	mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
}

t_env				ft_init_celt(void)
{
	t_env a;

	a.x1 = -2;
	a.x2 = 0.6;
	a.y1 = -1;
	a.y2 = 1.2;
	a.imax = 50;
	a.iimax = 0;
	a.zoom = 10;
	a.zooom = 1;
	a.x = 0;
	a.y = 0;
	a.nbcol = 0;
	a.width = 520;
	a.length = 480;
	a.imax = 50;
	a.rightleft = -0.5;
	a.updown = 0;
	a.zoom = 0;
	a.param = 0;
	return (a);
}

void				ft_celt1(t_env *a)
{
	a->image = mlx_new_image(a->mlx, a->width, a->length);
	a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
	mlx_string_put(a->mlx, a->win, 10, 10, 0xffffff, "Mandel");
	a->x1 = -2 - a->rightleft;
	a->x2 = 0.6 - a->rightleft;
	a->y1 = -1 - a->updown;
	a->y2 = 1.2 - a->updown;
	a->zoom = 200 * a->zooom;
	a->img_x = (a->x2 - a->x1) * a->zoom * a->zooom;
	a->img_y = (a->y2 - a->y1) * a->zoom * a->zooom;
	if (a->nbcol != 0)
	{
		a->color0 = rand() % 999999999;
		a->color = rand() % 999999999;
		a->color1 = rand() % 999999999;
	}
}

void				ft_celt(t_env *a)
{
	ft_celt1(a);
	a->x = 0;
	while (a->x++ < a->img_x && a->x <= a->width)
	{
		a->y = 0;
		while (a->y++ < a->img_y && a->y <= a->length)
		{
			a->zr = a->x / a->zoom + a->x1;
			a->zi = a->y / a->zoom + a->y1;
			a->cr = 0.45 + a->paramx;
			a->ci = 0 + a->paramy;
			a->i = -1;
			while (a->zr * a->zr + a->zi * a->zi < 4
					&& a->i++ < (a->imax + a->iimax))
			{
				a->tmp = a->zr;
				a->zr = a->zr * a->zr * a->zr - a->zr * a->zi * a->zi + a->cr;
				a->zi = 3 * (a->tmp * a->tmp * a->zi) - a->zi * a->zi *a->zi + a->ci;
			}
			ft_plot(a);
		}
	}
	mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
}

/*
**tricorne
*/

t_env				ft_init_ruby(void)
{
	t_env a;

	a.x1 = -2.1;
	a.x2 = 0.6;
	a.y1 = -1.2;
	a.y2 = 1.2;
	a.imax = 50;
	a.iimax = 0;
	a.zoom = 200;
	a.zooom = 1;
	a.x = 0;
	a.y = 0;
	a.nbcol = 0;
	a.width = 520;
	a.length = 480;
	a.imax = 50;
	a.rightleft = -0.5;
	a.updown = 0;
	a.zoom = 0;
	a.param = 0;
	return (a);
}

void				ft_init_ruby1(t_env *a)
{
	a->image = mlx_new_image(a->mlx, a->width, a->length);
	a->data = mlx_get_data_addr(a->image, &a->bpp, &a->size_line, &a->endian);
	mlx_string_put(a->mlx, a->win, 10, 10, 0xffffff, "Mandel");
	a->x1 = -2.1 - a->rightleft;
	a->x2 = 0.6 - a->rightleft;
	a->y1 = -1.2 - a->updown;
	a->y2 = 1.2 - a->updown;
	a->zoom = 200 * a->zooom;
	a->img_x = (a->x2 - a->x1) * a->zoom * a->zooom;
	a->img_y = (a->y2 - a->y1) * a->zoom * a->zooom;
	if (a->nbcol != 0)
	{
		a->color0 = rand() % 999999999;
		a->color = rand() % 999999999;
		a->color1 = rand() % 999999999;
	}
}

void				ft_ruby(t_env *a)
{
	ft_init_ruby1(a);
	a->x = 0;
	while (a->x++ < a->img_x && a->x <= a->width)
	{
		a->y = 0;
		while (a->y++ < a->img_y && a->y <= a->length)
		{
			a->cr = a->x / a->zoom + a->x1;
			a->ci = a->y / a->zoom + a->y1;
			a->zr = 0 + a->paramx;
			a->zi = 0 + a->paramy;
			a->i = -1;
			while (a->zr * a->zr + a->zi * a->zi < 4
					&& a->i++ < (a->imax + a->iimax))
			{
				a->tmp = a->zr;
				a->zr = a->zr * a->zr - a->zi * a->zi + a->cr;
				a->zi = -2 * a->tmp * a->zi + a->ci;
			}
			ft_plot(a);
		}
	}
	mlx_put_image_to_window(a->mlx, a->win, a->image, 0, 0);
}


/*
**fonction in order pto give the good direction
*/

void				ft_fct(t_env *a)
{
	if (a->nbfract == 1)
		ft_mandel(a);
	else if (a->nbfract == 2)
		ft_julia(a);
	else if (a->nbfract == 3)
		ft_ship(a);
	else if (a->nbfract == 4)
		ft_bunny(a);
	else if (a->nbfract == 5)
		ft_dendrite(a);
	else if (a->nbfract == 6)
		ft_heart(a);
	else if (a->nbfract == 7)
		ft_ruby(a);
	else if (a->nbfract == 8)
		ft_celt(a);
}

void				ft_error(void)
{
	t_env a;

	a.mlx = mlx_init();
	a.win = mlx_new_window(a.mlx, 500, 400, "mlx 42");
	a.image = mlx_new_image(a.mlx, 500, 400);
	mlx_string_put(a.mlx, a.win, 10, 30, 0xff00ff, "ERROR ! :-P");
	a.data = mlx_get_data_addr(a.image, &a.bpp, &a.size_line, &a.endian);
	mlx_string_put(a.mlx, a.win, 10, 10, 0x009999, "press ESC");
	mlx_pixel_put_to_image(a.x, a.y, &a);
	mlx_hook(a.win, 2, 6, my_key_funct, &a);
	sleep(1);
}

/*
**mlx_loop(a.mlx);
*/

int					main(int argc, char **argv)
{
	t_env			a;

	if (argc == 2)
	{
		if (ft_atoi(argv[1]) > 0 && ft_atoi(argv[1]) < 9)
			ft_mapping(argv[1]);
	}
	ft_error();
	ft_putstr("\033[92mCHOOSE A NUMBER\033[0m\n1 : Mandelbrot\n2 : Julia\n");
	ft_putstr("3 : Burningship: \n4 : Dentrite\n5 : Buggs Bunny\n6 : Bird of prey\n");
	ft_putstr("7 : tricorne\n8 : My spiral bud\n9 : ???\n");
	return (0);
}
