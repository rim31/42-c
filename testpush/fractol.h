# ifndef FRACTOL_H
# define FRACTOL_H
# include <fcntl.h>
# define PointerMotionHintMask	(1L<<7)
# define MotionNotify            6
# include <math.h>
#include <unistd.h>
#include "INCLUDES/libft.h"
#include "minilibx_macos/mlx.h"
//#include "mlx.h"

typedef struct	s_env
{
        int		bpp;
        int		size_line;
        int		color;
        int		color0;
        int		color1;
        int		nbcol;
		int		nbfract;
        char	*data;
        //int     *data;
        int		endian;
        void	*mlx;
        //void **mlx; //pour avoir *mlx[1]..8 et arriver jusqu'a 8fractales
		void	*win;
        void	*image;
        float	updown;
        float	rightleft;
        float	paramx;
		float	paramy;
		float	param;
        float	xi;
        float	xf;
        float	yi;
        float	yf;
        int		x;//coordonees du pixel
        int		y;
        float	z;//Nombre de Mandelbrot
        float	zi;
        float	zr;
        float	c;//constante de Mandelbrot
        float	ci;
        float	cr;
        int		i;//iteration
        int		imax;//iteration max
        int		iimax;
        //zone de dessin
        float	x1;
        float	x2;
        float	y1;
        float	y2;
        float	zoom;
        float	zooom;
        //definition de l'image
        float	img_x;
        float	img_y;
        float	tmp;
        float	width;
        float	length;
}				t_env;

void 			ft_fct(t_env *a);
t_env			ft_init_mandel(void);
void 			ft_init_mandel1(t_env *a);

void 			ft_julia(t_env *a);
int				ft_strnb(char const *s, char c);
int				my_key_funct(int keycode, t_env *a);
int 			get_next_line(int const fd, char **line);
void 			mlx_pixel_put_to_image(int x, int y, t_env *e);
int 			my_mouse_funct(int button, int x, int y, t_env *e);
int 			my_mouse_fct( int x, int y, t_env *e);
//t_env                   ft_mapping(t_env a);
t_env			ft_mapping(char *str);
void 			ft_mandel(t_env *a);
void 			ft_ship(t_env *a);
t_env			ft_init_ship(void);
void			ft_init_ship1(t_env *a);
void 			ft_bunny(t_env *a);
t_env			ft_init_bunny(void);
void			ft_init_bunny1(t_env *a);
void 			ft_dendrite(t_env *a);
t_env			ft_init_dendrite(void);
void			ft_init_dendrite1(t_env *a);
void 			ft_heart(t_env *a);
t_env			ft_init_heart(void);
void			ft_init_heart1(t_env *a);
void 			ft_ruby(t_env *a);
t_env			ft_init_ruby(void);
void			ft_init_ruby1(t_env *a);
void 			ft_celt(t_env *a);
t_env			ft_init_celt(void);
void			ft_init_celt1(t_env *a);

t_env                   ft_inival(void);
void			ft_plot(t_env *a);

#endif
